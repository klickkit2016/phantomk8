$(function() {
    init();
    paint();
    listen();
    $("#project-emp").fastselect();
    $("#taskemp").fastselect();

});

function init() {
    host = "http://192.168.1.3:4567/phantom/";
    user = { id: "1" };
    endpoints = {
        projectsOfMangaer: host + "list/projects/manager/",
        projectsOfEmployee: host + "list/projects/employee/",
        employeeOfProjects: host + "list/employees/",
        employeeOfCompany: host + "list/employees",
        addTask: host + "web/assign/task",
        addProject: host + "web/create/project",
        getTodo: host + "list/tasks/todo/",
        getDone: host + "list/tasks/done/",
        getDoing: host + "list/tasks/doing/",
    };
    state = { projectId: 1 };
}

function paint() {
    $.get(endpoints.projectsOfMangaer + user.id).done(function(data) {
        console.log("projectListAjax:", data);
        user.projectsOfEmployee = data;
        setProjectList(data);
    });
    $.get(endpoints.employeeOfCompany).done(function(data) {
        console.log("Overview Employee:", data);
        user.employeeOfCompany = data;
        setEmployeeList(data);
    });

}


function listen() {
    //Chat
    $(".send").click(function() {
        return addToChat($("textarea").val(), "left");
    });


    //Sidebar Tab Highlighter
    $(document).on('click', '#channel-block li:not(li:last-child)', function() {
        $("#channel-block li").removeClass("active");
        $(this).addClass("active");
        var projectId = $(this).data("project-id");
        if (projectId) {
            getEmployee(projectId);
            state.projectId = projectId;
            setCylinders();
        } else {
            setEmployeeList(user.employeeOfCompany);
            state.projectId = 0;
        }
    });
    $(document).on('click', '#employee-block li', function() {

    });

    // used in form pseudo checkbox
    $(".checker").click(function() {
        var name = $(this).children("input").attr('name');
        console.log(name);
        $("[name =" + name + " ]").prop("checked", "false").parents(".checker").removeClass("selected");
        $(this).addClass("selected");
        $(this).children("input[type=radio]").prop("checked", "checked");
    });

    //Search Icon --> close icon toggling/Reset
    $("#search").keyup(function() {
        if ($(this).val() === "") {
            $("#search-icon").removeClass("fa-close").addClass("fa-search");
        } else {
            $("#search-icon").removeClass("fa-search").addClass("fa-close").click(function() {
                $("#employee-block ul li").slideDown(100);
                $("#search-icon").removeClass("fa-close").addClass("fa-search");
                $("#search").val("");
            });
        }
        filterEmployee($(this).val().toUpperCase());
    });
    $(".addTaskDialog").click(function() {
        $("#task-assignment").removeClass("hide");
    });
    $(document).on('click', ".card div div", function() {
        console.log("clicked");
        var id = $(this).data("id");
        var tense = state.tense.doing.concat(state.tense.done, state.tense.todo);
        var result = $.grep(tense, function(e) { return e.id === id; });
        console.log(result, id);
        showTaskEnquiry(result);

    });


}

//Filter Employee List //OK
function filterEmployee(filter) {
    var searchList = $("#employee-block ul li");
    searchList.each(function(index, employee) {
        if ($(this).text().toUpperCase().indexOf(filter) > -1) {
            $(this).slideDown(100);
        } else {
            $(this).slideUp(100);
        }
    });
}

//Code Generators&Setters project arry -> dom
function setProjectList(projects) {
    var projectListDOM = "<li data-project-id='0' >Overview</li>";
    projects.forEach(function(project, index) {
        projectListDOM += "<li data-project-id=" + project.id + ">" + project.project_name + "</li>";
    });
    projectListDOM += "</br>"
    $("#channel-block ul").html(projectListDOM);
    /*
    projectListDOM += "<li class='uppercase addChannel' data-open='#add-channel-1'>+ Add Channel</li>"
    */
}
//employee array->dom
function setEmployeeList(employees) {
    var EmployeeListDOM = "";
    var EmployeeOptionDOM = "";
    employees.forEach(function(Employee) {
        EmployeeListDOM += "<li draggable='true' data-emp-id=" + Employee.id + " onDragStart='drag(event)'>" + Employee.user.username + "</li>";
        EmployeeOptionDOM += "<option value='" + Employee.id + "'>" + Employee.user.username + "</option>";
    });
    $("#employee-block ul").html(EmployeeListDOM);
    $("#project-emp").html(EmployeeOptionDOM);

}
//Fast select
function setFastSelect(employees) {
    optionsDOM = "";
    employees.forEach(function(employee) {
        optionsDOM += "<option value='" + employee.id + "'>" + employee.user.username + "</option>";
    });
    $("#taskemp").html(optionsDOM);
}

function getEmployee(id) {
    $.get(endpoints.employeeOfProjects + id).done(function(data) {
        console.log("Employee ajax", data);
        setFastSelect(data);
        setEmployeeList(data);
    });
}

function addToChat(message, position) {
    var chatMesage = "<div class='chatMessage " + position + "'>" + message + "</div>";
    $(".chatFlow").append(chatMesage);

}

//Drag & Drop
function allowDrop(event) {
    event.preventDefault();
}

function drag(event) {
    // event.preventDefault();
    event.dataTransfer.setData("text", event.target.innerHTML);
}

function drop(event) {
    // event.preventDefault();
    var name = event.dataTransfer.getData("text");
    $(event.target).append("<div>" + name + "</div>");
    console.log(name);
}

//listeners
//DOMgenerators
//contentproviders

function addTask(elem) {

    var obj = {};
    obj.project_id = state.projectId;
    obj.task_name = $("#task-assignment [name=name]").val();
    obj.deadline = $("#task-assignment [name=deadline]").val();
    obj.employee_id_list = $("#task-assignment [name=employees]").val();

    console.log(JSON.stringify(obj));

    $.post(endpoints.addTask, JSON.stringify(obj)).done(function(data) {
        $("#task-assignment").addClass("hide");
        alert("Task added");
        console.log(data);
    });


}

function addProject() {
    //{"project_name":"Project2","manager_id":"1","deadline":"2017-09-15","employee_id_list":["2"]}
    var obj = {
        project_name: $("#add-channel-1 [name=project-name]").val(),
        manager_id: user.id,
        deadline: $("#add-channel-1 [name=deadline]").val(),
        employee_id_list: $("#project-emp").val()
    };
    console.log("Add project object:", JSON.stringify(obj));

    $.post(endpoints.addProject, JSON.stringify(obj)).done(function(data) {
        console.log("Project Added");
        paint();
    });


}

function setCylinders() {
    $.when($.get(endpoints.getTodo + state.projectId), $.get(endpoints.getDoing + state.projectId), $.get(endpoints.getDone + state.projectId))
        .then(function(todo, doing, done) {
            console.log("set cylinders called");
            state.tense = {
                todo: todo[0],
                doing: doing[0],
                done: done[0]
            };
            console.log(state.tense);
            [todo[0], doing[0], done[0]].forEach(function(tense, index) {
                var taskDOM = "";
                tense.forEach(function(task) {
                    taskDOM += "<div class ='uppercase' data-id ='" + task.id + "'>" + task.task_name + "</div>";
                });
                switch (index) {
                    case 0:
                        $(".card.low div").html(taskDOM);
                    case 1:
                        $(".card.mid div").html(taskDOM);
                    case 2:
                        $(".card.high div").html(taskDOM);
                }
            });
        });
}

function showTaskEnquiry(task) {
    $("#task-enquiry").removeClass("hide");
}