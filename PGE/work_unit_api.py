from datetime import datetime
from datetime import timedelta
from dateutil import parser

from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.http import HttpApplicationError
from tastypie.http import HttpBadRequest
from tastypie.http import HttpNotFound
from tastypie.http import HttpUnauthorized
from tastypie.resources import ModelResource
from tastypie.utils import trailing_slash

from django.conf.urls import include
from django.conf.urls import url
from django.db.models import BooleanField
from django.db.models import Case
from django.db.models import When

from PGE import analytics_operations
from PGE import utils
from PGE.models import Employee
from PGE.models import Project
from PGE.models import Task


class EmployeeProjectResource(ModelResource):
    class Meta:
        queryset = Project.objects.all()
        resource_name ='projects'

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)%s$" %
            (self._meta.resource_name, trailing_slash()),
            self.wrap_view('get_employee_projects'),
            name="api_get_employee_projects"),
        ]

    def get_employee_projects(self, request, **kwargs):
        is_user_authentic, user = utils.is_user_authentic(request)
        if not is_user_authentic:
            return self.error_response(request,
                utils.get_error_response(
                'UnAuthorized',
                'User not allowed to access the resource'),
                HttpUnauthorized)
        try:
            employee = Employee.objects.get(user=user)
            projects = employee.projects.filter(end_date=None).annotate(
                is_working_project=Case(
                    When(working_employees=employee, then=True),
                    default=False,
                    output_field=BooleanField()
                )
            ).values('id', 'project_name', 'start_date', 'deadline', 'is_working_project')
            return self.create_response(request, list(projects))

        except Employee.DoesNotExist:
            return self.error_response(request,
                utils.get_error_response(
                'DoesNotExist',
                'Employee Does Not Exist'),
                HttpBadRequest
            )


class TaskResource(ModelResource):

    class Meta:
        queryset = Task.objects.all()
        resource_name ='task'
        excludes = ['eta']
        include_resource_uri = False

    def dehydrate_start_date(self, bundle):
        start_date = bundle.data['start_date']
        return start_date.strftime('%d/%m/%Y')

    def dehydrate_deadline(self, bundle):
        deadline = bundle.data['deadline']
        return deadline.strftime('%d/%m/%Y')

    def get_object_list(self, request):
        is_user_authentic, user = utils.is_user_authentic(request)
        if not is_user_authentic:
            return self.error_response(request,
                utils.get_error_response(
                'UnAuthorized',
                'User not allowed to access the resource'),
                HttpUnauthorized)
        project_id = request.GET.get('project_id')
        return super(TaskResource, self).get_object_list(request).filter(
            employees__user=user, project__id=project_id
        ).order_by('-tasklog__last_modified_at')

    def alter_list_data_to_serialize(self, request, data_dict):
        if isinstance(data_dict, dict):
            if 'meta' in data_dict:
                # Get rid of the "meta".
                del(data_dict['meta'])
        return data_dict
