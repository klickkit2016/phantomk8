from dateutil import parser

from django.test import TestCase

from analytics_operations import completed_and_remaining_tasks_of_day
from analytics_operations import get_tasks_in_period
from analytics_operations import project_progress
from factory_boy_analytics import ProjectFactory
from factory_boy_analytics import StageFactory
from factory_boy_analytics import TaskFactory
from factory_boy_analytics import TaskLogFactory
from PGE.models import Project
from PGE.models import Task
from PGE.models import TaskLog


class AnalyticsTestCase(TestCase):

    def test_get_tasks_in_period(self):
        project = ProjectFactory()
        start_date = parser.parse("19/1/2018")
        end_date = parser.parse("20/1/2018 23:59")
        project = ProjectFactory()
        stage_in_progress = StageFactory()
        stage_done = StageFactory(stage_name='103')
        task1 = TaskFactory(task_name='t1', project=project, stage=stage_in_progress)
        task2 = TaskFactory(task_name='t2', project=project, stage=stage_in_progress)
        task3 = TaskFactory(task_name='t3', project=project, stage=stage_in_progress)
        task4 = TaskFactory(task_name='t4', project=project, stage=stage_in_progress)
        task5 = TaskFactory(task_name='t5', project=project, stage=stage_in_progress)
        task6 = TaskFactory(task_name='t6', project=project, stage=stage_done)
        task7 = TaskFactory(task_name='t7', project=project, stage=stage_done)
        task8 = TaskFactory(task_name='t8', project=project, stage=stage_done)
        task9 = TaskFactory(task_name='t9', project=project, stage=stage_done)
        task10 = TaskFactory(task_name='t10', project=project, stage=stage_done)
        tl1 = TaskLogFactory(created_at=parser.parser("11/1/2018"), task=task1)
        tl2 = TaskLogFactory(created_at=parser.parser("12/1/2018"), task=task2)
        tl3 = TaskLogFactory(created_at=parser.parser("13/1/2018"), task=task3)
        tl4 = TaskLogFactory(created_at=parser.parser("14/1/2018"), task=task4)
        tl5 = TaskLogFactory(created_at=parser.parser("15/1/2018"), task=task5)
        tl6 = TaskLogFactory(created_at=parser.parser("16/1/2018"), task=task6)
        tl7 = TaskLogFactory(created_at=parser.parser("17/1/2018"), task=task7)
        tl8 = TaskLogFactory(created_at=parser.parser("18/1/2018"), task=task8)
        tl9 = TaskLogFactory(created_at=parser.parser("19/1/2018"), task=task9)
        tl10 = TaskLogFactory(created_at=parser.parser("20/1/2018"), task=task10)
        tasks = get_tasks_in_period(project, start_date, end_date)
        self.assertEqual(tasks[0], task9)

