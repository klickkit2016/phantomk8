from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.http import HttpBadRequest
from tastypie.http import HttpNotFound
from tastypie.http import HttpUnauthorized
from tastypie.resources import ModelResource
from tastypie.utils import trailing_slash

from django.conf.urls import include
from django.conf.urls import url
from django.contrib.auth.models import User
from django.http import HttpResponse

from PGE import constants
from PGE import utils
from PGE.models import Employee



class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        include_resource_uri = False
        excludes = [
            'date_joined', 'is_active', 'is_staff',
            'is_superuser', 'last_login', 'last_name',
            'password', 'username', 'id'
        ]

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/get/api-key%s$" %
            (self._meta.resource_name, trailing_slash()),
            self.wrap_view('get_api_key'),
            name="api_get_api_key"),
        ]

    def get_api_key(self, request, **kwargs):
        is_user_authentic, user = utils.is_user_authentic(request)
        if not is_user_authentic or not user.is_staff:
            return self.error_response(request,
                utils.get_error_response(
                'UnAuthorized',
                'User not allowed to access the resource'),
                HttpUnauthorized)
        email = request.GET.get('email')
        try:
            response_list = []
            users = User.objects.filter(email=email).select_related('employee')
            for user in users:
                try:
                    team = user.employee.team.all().first()
                    team_name = team.team_name
                    response_dict = {'team_name': team_name, 'api_key': user.api_key.key}
                    response_list.append(response_dict)
                except Employee.DoesNotExist:
                    continue
            return self.create_response(request, response_list)
        except User.DoesNotExist:
            return self.error_response(request,
                utils.get_error_response(
                'DoesNotExist',
                'User Does Not Exist'),
                HttpBadRequest
            )
