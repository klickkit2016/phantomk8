from django.conf.urls import include
from django.conf.urls import url

from tastypie.api import Api

from PGE.account_api import UserResource
from PGE.analytics_api import AnalyticsResource
from PGE.dashboard_apis import ProjectResource
from PGE.employee_api import EmployeeResource
from PGE.event_api import EventResource
from PGE.lead_api import LeadResource
from PGE.link_archive_api import LinkResource
from PGE.views import get_access_tokens
from PGE.views import handle_message
from PGE.views import handle_oauth_flow
from PGE.views import handle_slack_interaction
from PGE.views import handle_slash_command
from PGE.views import render_home
from PGE.work_unit_api import EmployeeProjectResource
from PGE.work_unit_api import TaskResource


v1_api = Api(api_name='v1')
v1_api.register(UserResource())
v1_api.register(AnalyticsResource())
v1_api.register(EmployeeResource())
v1_api.register(EventResource())
v1_api.register(LeadResource())
v1_api.register(LinkResource())
v1_api.register(ProjectResource())
v1_api.register(EmployeeProjectResource())
v1_api.register(TaskResource())


urlpatterns = [
    url(r'^api/', include(v1_api.urls)),
    url(r'^list/access/tokens', get_access_tokens, name='get_access_tokens'),
    url(r'^slack/interaction', handle_slack_interaction, name='handle_slack_interaction'),
    url(r'^oauth', handle_oauth_flow, name='handle_oauth_flow'),
    url(r'^submitMessage', handle_message, name="handle_message"),
    url(r'description', render_home, name='render_home'),
    url(r'slash-command', handle_slash_command, name='handle_slash_command'),
    ]
