import csv
import httplib2
import json
import pyrebase
import pytz
import os.path
import sys
import random
import requests

from rest_framework.parsers import JSONParser

from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone

from PGE.models import Absence
from PGE.models import AccessGroup
from PGE.models import Discussion
from PGE.models import Employee
from PGE.models import Link, Manager, Preference, Priority, Pipe, Project, Role, Selection, Session, Task, Team, Disjunct, Stage
from PGE.models import CompletionMark
from PGE.models import MessageLog
from PGE.serializers import DisjunctSerializer, EmployeeSerializer, ManagerSerializer, LinkSerializer, ProjectSerializer, TaskSerializer, TeamSerializer
from PGE.tasks import notify_employees
from PGE.tasks import notify_new_team
from PGE.tasks import send_email

from datetime import datetime, timedelta, time


from dateutil import parser

from apiclient.discovery import build

from oauth2client.service_account import ServiceAccountCredentials

from PhantomGabEngine.settings import DEFAULT_FROM_EMAIL

from PGE.constants import CLIENT_ID
from PGE.constants import CLIENT_SECRET

from PGE import analytics_operations
from PGE import constants
from PGE import operations
from PGE import utils

import logging
logger = logging.getLogger(__name__)


try:
    import apiai
except ImportError:
    sys.path.append(
        os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir)
    )
    import apiai

CLIENT_ACCESS_TOKEN = '871cec3a929446928dd3f67ded30781d'
SESSION_ID = 1001

service_account_email = 'calendar@phantom-gab-engine.iam.gserviceaccount.com'

CLIENT_SECRET_FILE = 'PGE/calendar_service_account.json'

SCOPES = 'https://www.googleapis.com/auth/calendar'
scopes = [SCOPES]
tz = pytz.timezone('Asia/Kolkata')



config = {
  "apiKey": "AIzaSyB4555K4PmN7z5oMFIIfu08HSV_NRReSZQ",
  "authDomain": "phantom-gab-engine.firebaseapp.com",
  "databaseURL": "https://phantom-gab-engine.firebaseio.com",
  "storageBucket": "phantom-gab-engine.appspot.com",
  "serviceAccount": "PGE/phantom-gab-engine-firebase-adminsdk-o9tcv-6faea27d58.json"
}

firebase = pyrebase.initialize_app(config)
auth = firebase.auth()
firebase_user = auth.sign_in_with_email_and_password("sriablaze@gmail.com", "1234sri#")
db = firebase.database()

# Utility method to delete unicodes


CONTEXT_TASK_ASSIGNMENT = 'TASK_ASSIGNMENT'
MEETING_CALLBACK_ID = 'MEET/'
LEAVE_REQUEST_CALLBACK_ID = 'LEAVE/'


BUTTON_OPTION_YES = 'Yes'
BUTTON_OPTION_NO = 'No'


WORK_QUERY_INTENT = 'work_query'
EVENT_TYPE_DIRECT_MESSAGE = 'direct_message'
ADMIN_GROUP_NAME = 'ADMIN_GROUP'
PROVIDE_ADMIN_ACCESS_INTENT = 'provide_admin_access'
CONTEXTS_DELETE_ENDPOINT = 'https://api.api.ai/v1/contexts/{0}?sessionId={1}'
CONTEXTS_ENDPOINT = 'https://api.api.ai/v1/contexts?sessionId={0}'
OAUTH_SLACK_INITIATION_URL = 'https://slack.com/oauth/authorize?scope=identity.basic,identity.team,identity.avatar&client_id={0}'.format(CLIENT_ID)
CREATE_TASK_INTENT = 'create_task'
CREATE_PROJECT_INTENT = 'create_project'
DATE_PERIOD_KEY = 'date_period'
DATE_KEY = 'date'
GITHUB_BASE_ENDPOINT = 'https://api.github.com'
TODO_STAGE_NAME = '101'
DOING_STAGE_NAME = '102'
DONE_STAGE_NAME = '103'
TASK_ADDITION_KEY_PROJECT_NAME = "project_name"
TASK_ADDITION_KEY_TASKS = "tasks"
TASK_ADDITION_MANAGER_EMAIL = "manager_email"
TASK_ENTITY_NAME = "Task"
DISJUNCT_ENTITY_NAME = "Disjunct"
EMPLOYEE_ENTITY_NAME = "employee"
PROJECT_ENTITY_NAME = "Project"
TASK_ENTITY_ADDITION_URL = "https://api.api.ai/v1/entities/{0}/entries?v=20150910".format(TASK_ENTITY_NAME)
DISJUNCT_ENTITY_ADDITION_URL = "https://api.api.ai/v1/entities/{0}/entries?v=20150910".format(DISJUNCT_ENTITY_NAME)
EMPLOYEE_ADDITION_URL = "https://api.api.ai/v1/entities/{0}/entries?v=20150910".format(EMPLOYEE_ENTITY_NAME)
PROJECT_ADDITION_URL = "https://api.api.ai/v1/entities/{0}/entries?v=20150910".format(PROJECT_ENTITY_NAME)
MESSAGE_SUBMISSION_URL = "https://api.api.ai/v1/query?v=20150910"
SUCCESS_STATUS_CODE = 200
MESSAGE_REQUEST_KEY = "message"
ACTION_INCOMPLETE = "actionIncomplete"
RESULT_KEY = "result"
DATE_DEADLINE_KEY = "date_deadline"
DURATION_DEADLINE_KEY = "duration_deadline"
headers = {'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer b55df5347afe4002a39e94cd61c121c9'}
WORK_START_INTENT_NAME = "work_start"
SESSION_START_INTENT_NAME = "session_start"
SESSION_END_INTENT_NAME = "session_end"
BREAK_START_INTENT_NAME = "break_start"
BREAK_END_INTENT_NAME = "break_end"
TASK_KEY = "task"
CONTEXT_DELETE_TASK_CONFIRMATION = "delete_task_confirmation"
DISJUNCT_NAME_KEY = "Disjunct"
MEETING_INTENT_NAME = "meet_schedular"
ASSIGNMENT_INTENT_NAME = "Assignment"
ASSIGNMENT_CONTINUATION_INTENT_NAME = "assignment_continuation"
LEAVE_ABSENCE_REQUEST_INTENT = "set_absence"
LEAVE_REQUEST_ACCEPTANCE_INTENT = "leave_reply_acceptance"
LEAVE_REQUEST_DENIAL_INTENT = "leave_request_denial"
SET_DISCUSSION_PREFERENCE_INTENT = "discussion_preference"
DISCUSSION_SCHEDULE_REQUEST_INTENT = "discussion_schedule_request"
ADD_DISJUNCTS_TO_TASK_FORMAT_REQUEST = "create_disjuncts_to_task"
GROUP_DISJUNCT_ADDITIONS = "group_disjuncts_addition"
TASK_LIST_REQUEST = "task_list_request"
LIST_TASKS_REQUEST_TODO = 'list_tasks_request_to_do'
LIST_TASKS_REQUEST_DOING = 'list_tasks_request_doing'
LIST_TASKS_REQUEST_DONE = 'done_task_list_request'
SHIFT_TASK_REQUEST = 'shift_task_request'
SCHEDULE_BREAKS_INTENT_NAME = 'schedule_breaks'
SET_WORK_FROM_HOME = 'set_work_from_home'
SWITCH_PROJECT_INTENT_NAME = "switch_project"
project_agnostic_intents = {CREATE_PROJECT_INTENT: None, PROVIDE_ADMIN_ACCESS_INTENT: None, SWITCH_PROJECT_INTENT_NAME: None}
REGULAR_NEW_SESSION = 'REG'
SWITCHED_NEW_SESSION = 'SWITCH'
SET_PROGRESS_INTENT = 'set_progress_mark'
SHOW_CURRENT_TASK_IN_TIMER = 'show_current_task_in_timer'
GET_THE_CURRENT_WORKING_PROJECT = 'get_current_working_project'
GET_PROGRESS_OF_TASK = 'get_progress_of_task'
SCHEDULE_DISCUSSION = 'schedule-discussion'
ETA_SET_BY_USER = 'user_sets_eta_by_force'
PROGRESS_SET_BY_USER = 'user_sets_progress_by_force'
USER_ACCEPTS_DELETION_INTENT = "user_accepts_deletion"
CHANGE_DEADLINE_OF_TASK = 'change_deadline_of_task'
SCHEDULE_NEXT_TASK = "next_task_to_work"
TASK_TIME_QUERY = "task_time"
GET_LINKS = "get_links"
RANDOM_SESSION_START = "random_session_start"
CREATE_TASK_FOLLOWUP_CONTEXT = "create_task-followup"
CREATE_TASK_DETAILS_ENTRY_INTENT = "create_task-details_entry"


def build_service():
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        filename=CLIENT_SECRET_FILE,
        scopes=SCOPES
    )

    http = credentials.authorize(httplib2.Http())

    service = build('calendar', 'v3', http=http)

    return service


def create_event(deadline, summary, description):
    service = build_service()
    start_datetime = datetime.now(tz=tz)
    event = service.events().insert(calendarId='sricharanprograms@gmail.com', body={
        'summary': summary,
        'description': description,
        'start': {'dateTime': start_datetime.isoformat()},
        'end': {'dateTime': deadline.isoformat()},
    }).execute()

    print(event)



def byteify(input):
    if isinstance(input, dict):
        return {byteify(key): byteify(value)
                for key, value in input.items()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, bytes):
        return input.encode('utf-8')
    else:
        return input


def call_api(session_id, query):
    ai = apiai.ApiAI(CLIENT_ACCESS_TOKEN)

    request = ai.text_request()

    request.session_id = session_id

    request.query = query

    response = request.getresponse()

    return response.read()


def send_automated_message_to_nlp(message, employees):
    for employee in employees:
        print('At automation')
        print(employee.unique_id)
        request_dict = {"query" : [message], "sessionId" : employee.unique_id, "lang" : "en" , "timezone" : "Asia/Colombo"}
        request_dict = json.dumps(request_dict)
        print(request_dict)
        response = requests.post(MESSAGE_SUBMISSION_URL, data=request_dict, headers=headers)
        response_dict = byteify(response.json())
        print(response_dict)


def add_user_to_agent(user_name):
    headers = {'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer b55df5347afe4002a39e94cd61c121c9'}
    request_dict = {"value" : user_name, "synonyms" : [user_name]}
    request_dict = json.dumps(request_dict)
    entity_request = requests.post(EMPLOYEE_ADDITION_URL, data=request_dict, headers=headers)
    print(entity_request.json())
    if entity_request.status_code == SUCCESS_STATUS_CODE:
        print("Entity added successfully")


def add_entries_to_agent(task_names):
    headers = {'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer b55df5347afe4002a39e94cd61c121c9'}
    request_list = []
    for task_name in task_names:
        request_dict = {"value" : task_name, "synonyms" : [task_name]}
        request_list.append(request_dict)
    request_list = json.dumps(request_list)
    entity_request = requests.post(DISJUNCT_ENTITY_ADDITION_URL, data=request_list, headers=headers)
    print(entity_request.json())
    if entity_request.status_code == SUCCESS_STATUS_CODE:
        print("Entity added successfully")

@csrf_exempt
def add_tasks(request):
    if request.method == 'POST':
        selection_dict = {}
        task_names = []
        recieved_json = json.loads(request.body)
        print(recieved_json)
        recieved_dict = byteify(recieved_json)
        channel_name = recieved_dict['channel_name']
        manager_email = recieved_json['manager_email']
        user = User.objects.get(email=manager_email)
        employee_obj = user.employee
        manager_obj, created = Manager.objects.get_or_create(employee_instance=employee_obj)
        print(created)
        print(Manager.objects.all())
        manager_obj.project_set.create(project_name=channel_name)
        print(manager_obj.project_set.all())
        project_obj = Project.objects.get(project_name=channel_name)
        print(recieved_dict)
        for task_obj in recieved_dict['tasks']:
            task_name = task_obj['task_name']
            task_names.append(task_name)
            project_obj.task_set.create(task_name=task_name)

        print(project_obj.task_set.all())
        for role_emp_object in recieved_dict["employees"]:
            role_name = role_emp_object['role_name']
            employee_email = role_emp_object['employee']['email']
            print(employee_email)
            role_obj = Role.objects.get(role_name=role_name)
            user = User.objects.get(email=employee_email)
            employee_obj = user.employee
            project_obj.employees.add(employee_obj)
            selection_obj, created = project_obj.selections.get_or_create(role=role_obj)
            selection_obj.employees.add(employee_obj)
            selection_dict[role_name] = selection_obj
        print(selection_obj.employees.count())
        print(selection_dict)
        for role, selection_obj in selection_dict.items():
            project_obj.selections.add(selection_obj)
        entity_entries = []
        entity_name = TASK_ENTITY_NAME
        request_list = []
        for task_name in task_names:
            request_dict = {"value" : task_name, "synonyms" : [task_name]}
            request_list.append(request_dict)

        print(request_list)
        request_list = json.dumps(request_list)
        entity_request = requests.post(TASK_ENTITY_ADDITION_URL, data=request_list, headers=headers)
        print(entity_request.json())
        if entity_request.status_code == SUCCESS_STATUS_CODE:
            print("Entity added successfully")

        return HttpResponse(status=200)
    else:
        return HttpResponse(status=403)


def _days_hours_minutes(td):
    return td.days, td.seconds//3600, (td.seconds//60)%60


def format_date_hours_minutes_worked(days, hours, minutes):
    response_string = ""
    is_hour_data_present = False
    if hours != 0:
        is_hour_data_present = True
        if hours > 1:
            response_string = "{0} hours".format(hours)
        else:
            response_string = "{0} hour".format(hours)

    if minutes != 0:
        if is_hour_data_present:
            response_string = response_string + " " + "and"

        if minutes > 1:
            response_string = response_string + " " + "{0} minutes".format(minutes)
        else:
            response_string = response_string + " " + "{0} minute".format(minutes)

    return response_string


def _format_end_session_response(days, hours, minutes, task_name):
    if hours == 0 and minutes == 0:
        response = "You haven't worked for a minute."
    else:
        work_time_string = format_date_hours_minutes_worked(days, hours, minutes)
        response = "You've worked on `{0}` for {1}".format(task_name, work_time_string)
    response = {
        "speech_response" : response
    }
    return response

def send_response(results_dict):
    fulfillment = results_dict['fulfillment']
    speech_response = fulfillment['speech']
    response = {
        "speech_response" : speech_response
    }
    return response


def add_additional_response(base_agent_response, additional_response):
    base_agent_response["speech_response"] = (
        additional_response["speech_response"] +
        '\n' +
        base_agent_response["speech_response"]
    )
    return HttpResponse(json.dumps(base_agent_response), content_type="application/json")


def end_work(request, user):
    response = operations.end_work(user)
    return utils.render_custom_response(response)

def shift_tasks(employee, result_parameters, default_response):
    task_names = result_parameters['Task']
    status, any_response = operations.shift_tasks(
        employee,
        task_names
    )
    print(status)
    if not status:
        return render_custom_response(any_response)

    if any_response:
        return render_custom_response(
            any_response +
            '\n' +
            default_response
        )
    else:
        return render_custom_response(default_response)


def compute_progress_score(employee, days=7):
    start_of_week_date_time = datetime.now() - timedelta(days=days)
    total_number_of_tasks_taken_up = Task.objects.filter(created_at__gte=start_of_week_date_time).count()
    total_number_of_tasks_done = Task.objects.filter(created_at__gte=start_of_week_date_time,
        stage__stage_name=DONE_STAGE_NAME
    ).count()
    return (total_number_of_tasks_done / total_number_of_tasks_taken_up)


def write_to_csv(task_dict, username, score):
    date = str(datetime.now())
    file_name_string = "Weekly report for {0} - {1}.csv".format(username, date)
    score_string = "Progress score: {}".format(int(score))
    with open(file_name_string, 'w') as myfile:
        writer = csv.writer(myfile, quoting=csv.QUOTE_ALL)
        writer.writerow(['S.no', 'Task', 'Project', 'Stage', 'Spent time', 'Number of work sessions'])
        for key, value in task_dict.items():
            writer.writerow(value)
        for i in range(2):
            writer.writerow([])
        writer.writerow([score_string])
    return file_name_string


def send_report_email(employee, file_name_string):
    start_date_for_report = (timezone.now() - timedelta(days=7)).date().strftime('%d/%m/%Y')
    end_date_for_report = timezone.now().date().strftime('%d/%m/%Y')
    recipient_list = []
    managers = employee.working_project.managers.all()
    for manager in managers:
        recipient_list.append(manager.employee_instance.user.email)
    subject = "Productivity Report cycle: {0} - {1}".format(str(start_date_for_report), str(end_date_for_report))
    body = "Please find the attached report that depicts the productivity of {} this week."\
        .format(employee.user.first_name)
    send_email.delay(subject, body, recipient_list, attachment_file_name=file_name_string)



def generate_report(employee, days=7):
    start_date_time = datetime.now() - timedelta(days=days)
    all_tasks = employee.task_set.filter(session__start_date_time__gte=start_date_time)\
        .exclude(stage__stage_name=TODO_STAGE_NAME).prefetch_related('session_set')
    task_dict = {}
    for index,task in enumerate(list(all_tasks)):
        task_name = task.task_name
        stage_name_intuitive = STAGE_MAPPING[task.stage.stage_name]
        task_dict[task_name] = [index + 1, task_name, task.project.project_name, stage_name_intuitive]
        print(task_dict[task_name])
        session_timedelta = timedelta(0, 0, 0)
        sessions = task.session_set.filter(start_date_time__gte=start_date_time)
        for session in sessions:
            if session.end_date_time:
                session_timedelta = session_timedelta + (timezone.localtime(session.end_date_time) - timezone.localtime(session.start_date_time))
            else:
                session_timedelta = session_timedelta + (timezone.now() - timezone.localtime(session.start_date_time))
        days, hours, minutes = _days_hours_minutes(session_timedelta)
        response = _format_end_session_response(days, hours, minutes, task_name, for_report=True)
        task_dict[task_name].append(response)
        task_dict[task_name].append(len(sessions))
        score = compute_progress_score(employee)
    if all_tasks:
        file_name_string = write_to_csv(task_dict, employee.user.first_name, score)
        send_report_email(employee, file_name_string)

'''
def send_weekly_reports():
    one_week_back_date = timezone.now() - timedelta(days=constants.NUMBER_OF_DAYS_IN_WEEK)
    teams = Team.objects.all().prefetch_related('project_set')
    for team in teams:
        days = None
        if team.joined_at < one_week_back_date:
            days = (timezone.now() - team.joined_at).days
        all_projects = team.project_set.all().prefetch_related('employee_set')
        for project in all_projects:
            for employee in project.employee_set.all():
                if days:
               generate_report(employee)
'''


def start_break(request, user, results_dict):
    status, error_message = operations.close_session(user)
    if status:
        return render_custom_response(utils.get_default_response(results_dict))
    else:
        return render_custom_response(error_message)


def end_break(request, user):
    operations.resume_work(request, user)


def get_progress_of_task(employee, result_parameters):
    task_name = result_parameters.get('Task')
    operation_did_succeed, response = operations.get_progress_of_task(employee, task_name)
    if operation_did_succeed:
        completion_value = str(response) + "%"
        response = "`{0}` of `{1}` is completed.".format(completion_value, task_name)
    else:
        if response == constants.QUERY_NO_MATCH_MESSAGE:
            response = constants.message_for_task_not_in_doing_list(task_name)
        else:
            response = "You haven't set the progress mark for `{0}`.".format(task_name)
    return render_custom_response(response)


def set_discussion_preference(employee, preference_start_time, preference_end_time):
    preference, created = Preference.objects.get_or_create(employee=employee)
    preference.from_date_time = preference_start_time
    preference.to_date_time = preference_end_time
    preference.save()
    pref = employee.preference



def create_meeting_event(meet_time, summary, description, attendees=None):
    service = build_service()
    end_time = meet_time + timedelta(hours=1)
    event = service.events().insert(calendarId='sricharanprograms@gmail.com', body={
        'summary': summary,
        'description': description,
        'start': {'dateTime': meet_time.isoformat()},
        'end': {'dateTime': end_time.isoformat()},
        'attendees': attendees
    }).execute()

    print(event)


def schedule_meeting(meet_time, meet_venue, attendees_email_list, organizer_email):
    attendees = []
    meet_time = parser.parse(meet_time)
    meet_time = meet_time - timedelta(hours=5, minutes=30)
    meet_time = meet_time.astimezone(tz)
    print("The meet time is {0}".format(meet_time))
    for attendee_email in attendees_email_list:
        each_mail_dict  = {'email' : attendee_email}
    attendees.append(each_mail_dict)
    username = User.objects.get(email=organizer_email).username
    summary = "Meeting organized by {0}".format(username)
    description = "Weekly meetup"
    create_meeting_event(meet_time, summary, description, attendees)


def create_attendence_tracker():
    managers = Manager.objects.all()
    for manager in managers:
        manager_email = manager.employee_instance.user.email
        at_stripped_email = manager_email.replace("@", "")
        dot_stripped_email = at_stripped_email.replace(".", "")
        input_dict = {"name" : "Phantom Attendence Tracker"}
        unique_key = "PAT{0}".format(dot_stripped_email)
        db.child("master").child("channels").child(unique_key).set(input_dict, firebase_user['idToken'])
        channel_dict = {"channel_id" : unique_key}
        db.child("master").child(dot_stripped_email).child("associated_rooms").push(channel_dict, firebase_user['idToken'])


def leave_request(request, start_date, end_date, manager_email, user):
    at_stripped_email = manager_email.replace("@", "")
    dot_stripped_email = at_stripped_email.replace(".", "")
    unique_key = "PAT{0}".format(dot_stripped_email)
    username = user.username
    days, minutes, hours = _days_hours_minutes(end_date - start_date)
    message_text = "Hey, {0} has requested leave of absence for {1} days from {2} to {3}".format(user.first_name, days + 1, str(start_date), str(end_date))
    message_dict = {"senderId" : "phantom", "senderName" : "phantom", "text": message_text}
    channel_dict = db.child("master").child("channels").child(unique_key).child("messages").push(message_dict, firebase_user['idToken'])


def return_min_and_max_date_tuples(date_tuple_one, date_tuple_two, index=0):
    if date_tuple_one[index].time() > date_tuple_two[index].time():
        return date_tuple_two, date_tuple_one
    elif date_tuple_two[index].time() > date_tuple_one[index].time():
        return date_tuple_one, date_tuple_two
    else:
        return date_tuple_one, date_tuple_two


def find_intersection(date_range_list):
    is_full_intersection = True

    intersection_tuple = date_range_list[0]
    for date_tuple in date_range_list[1:]:

        min_tuple, max_tuple = return_min_and_max_date_tuples(intersection_tuple, date_tuple)
        if max_tuple[0] < min_tuple[1]:
            start_date_time = max_tuple[0]
            min_end_date_tuple, max_end_date_tuple = return_min_and_max_date_tuples(min_tuple, max_tuple, index=1)
            min_date_time = min_end_date_tuple[1]
            intersection_tuple = (start_date_time, min_date_time)
        else:
            is_full_intersection = False
            break

    return (is_full_intersection, intersection_tuple)



# Basically we apply the nCr formula where we apply the r in reverse order
def generate_combinations(request, index_list,  n,  r, index, combination, i, date_range_list):

    set_preference = request.session['set_preference']

    if index == r:
        date_tuple_list = []

        if not set_preference:
            for combination_index in combination:
                date_tuple_list.append(date_range_list[combination_index])

            is_intersecting, intersection_tuple = find_intersection(date_tuple_list)
            request.session['set_preference'] = is_intersecting

            if request.session['set_preference']:
                request.session['intersection_tuple'] = intersection_tuple
        return
    if i >= n:
        return
    combination[index] = index_list[i]
    generate_combinations(request, index_list, n, r, index + 1, combination, i + 1, date_range_list)
    generate_combinations(request, index_list, n, r, index, combination, i + 1, date_range_list)


def intersection_helper(request, date_range_list):
    complete_intersection = False
    request.session['intersection_tuple'] = (0, 0)
    request.session['set_preference'] = False

    date_tuple_list = []
    size = len(date_range_list)
    index_list = [i for i in range(0, size)]
    r = size
    while not request.session['set_preference'] and r > 1:
        combination_list = [0 for i in range(0, r)]
        generate_combinations(request, index_list, size, r, 0, combination_list, 0, date_range_list)
        r = r - 1
    if r == size - 1:
        complete_intersection = True

    if request.session['set_preference']:
        print("I am here")
        return (request.session['intersection_tuple'], complete_intersection)
    else:
        return (None, complete_intersection)


def render_interactive_request_for_slack(text, title, callback_id):
    text = text
    actions = [
        {
            "name": "Yes",
            "text": "Yes",
            "type": "button",
            "value": "Yes"
        },
        {
            "name": "No",
            "text": "No",
            "type": "button",
            "value": "No"
        }

    ]
    title = title
    callback_id = callback_id
    attachment_type = 'default'
    inner_response_dict = {'title':title, 'callback_id':callback_id, 'attachment_type':attachment_type, 'actions':actions}
    outer_response_dict = {'text': text, 'attachments':[inner_response_dict]}
    return outer_response_dict


def create_task(employee, result_parameters):
    task_name = result_parameters.get('task')
    deadline_dict = result_parameters.get("deadline")
    deadline = utils.parse_deadline(deadline_dict)
    eta_dict = result_parameters.get("required_time")
    eta_value_in_hours = utils.get_deadline_in_hours(eta_dict)
    task, response_dict = operations.create_task(
        employee,
        task_name,
        deadline,
        eta_value_in_hours
    )
    if task:
        '''
        utils.schedule_notification_for_nearing_deadline(
            employee,
            task_name,
            deadline
        )
        '''
        operations.schedule_reminder_for_task_completion_check(
            task.id,
            employee.id,
            deadline
        )
        return (True, response_dict)


def user_updates_eta(employee, result_parameters):
    task_name = result_parameters.get('task')
    eta_dict = result_parameters.get('eta')
    completion_mark = result_parameters.get('progress')
    completion_value = utils.parse_progress_dict(completion_mark)
    operations.set_progress_for_task(employee, task_name, completion_value)
    eta_value_in_hours = utils.get_deadline_in_hours(eta_dict)
    status, error_code = operations.set_new_eta_for_task(employee, task_name, eta_value_in_hours)
    if not status:
        if error_code == constants.DOES_NOT_EXIST_ERROR_CODE:
            response = constants.message_for_task_not_in_doing_list(task_name)
            return (False, response)
    else:
        return (True, None)


def list_todo_tasks(employee):
    task_name_list_to_be_printed = []
    todo_tasks = operations.filter_tasks_in_state(employee, [constants.TODO_STAGE_NAME])
    if not todo_tasks:
        the_list_string = "You don't have any task that you haven't started working on."
    else:
        the_header_string = "The tasks to do are:"
        task_name_list_to_be_printed.append(the_header_string)
        for (index, task) in enumerate(todo_tasks):
            the_new_list_element = str(index + 1) + '.' + " " + task.task_name
            task_name_list_to_be_printed.append(the_new_list_element)
        the_list_string = '\n'.join(task_name_list_to_be_printed)
    return render_custom_response(the_list_string)


def list_doing_tasks(employee):
    task_name_list_to_be_printed = []
    doing_tasks = operations.filter_tasks_in_state(employee, [constants.DOING_STAGE_NAME])
    if not doing_tasks:
        the_list_string = "You don't have any task that you are working on."
    else:
        the_header_string = "The tasks in progress are:"
        task_name_list_to_be_printed.append(the_header_string)
        for (index, task) in enumerate(doing_tasks):
            the_new_list_element = str(index + 1) + '.' + " " + task.task_name
            task_name_list_to_be_printed.append(the_new_list_element)
        the_list_string = '\n'.join(task_name_list_to_be_printed)
    return render_custom_response(the_list_string)


def list_done_tasks(employee):
    task_name_list_to_be_printed = []
    done_tasks = operations.filter_tasks_in_state(employee, [constants.DONE_STAGE_NAME])
    if not done_tasks:
        the_list_string = "You don't have any task that is completed."
    else:
        the_header_string = "The following tasks are completed: "
        task_name_list_to_be_printed.append(the_header_string)
        for (index, task) in enumerate(done_tasks):
            the_new_list_element = str(index + 1) + '.' + " " + task.task_name
            task_name_list_to_be_printed.append(the_new_list_element)
        the_list_string = '\n'.join(task_name_list_to_be_printed)
    return render_custom_response(the_list_string)


def start_work(employee, result_parameters):
    non_processed_task_name = result_parameters['task']
    task_name = utils.preprocess_names(non_processed_task_name)
    task = operations.move_task_to_state_and_return_task(employee,
        task_name,
        constants.DOING_STAGE_NAME,
        [constants.TODO_STAGE_NAME, constants.DOING_STAGE_NAME]
    )
    action_during_creation, switch_response = operations.create_new_session(
       employee.user,
        task
    )
    response = 'I have started your work session on `{0}`.'.format(task_name)
    if action_during_creation == SWITCHED_NEW_SESSION:
        return switch_response + '\n' + response
    else:
        return response


def get_task_time(employee, result_parameters):
    task_name = result_parameters['Task']
    status, payload = operations.get_task_time(employee, task_name)
    return(status, payload)


def schedule_discussion(organizer, result_parameters):
    employee_ids = result_parameters['employee']
    meet_time_string = result_parameters['meet_time']
    duration = result_parameters['duration']
    meet_date_time = parser.parse(meet_time_string, ignoretz=True)
    # Check if a default time is set
    if meet_date_time.time() == datetime.now().time().min:
        default_date_time = parser.parse(constants.DEFAULT_TIME_STRING)
        meet_date_time = datetime.combine(
            meet_date_time.date(),
            default_date_time.time()
        )
    meet_date_time = constants.tz.localize(meet_date_time)
    amount = duration["amount"]
    unit = duration["unit"]
    if unit == "h":
        approximate_meet_end_time = meet_date_time + timedelta(hours=int(amount))
    else:
        approximate_meet_end_time = meet_date_time + timedelta(minutes=int(amount))
    status, response_value = operations.schedule_discussion(
        organizer,
        employee_ids,
        meet_date_time,
        approximate_meet_end_time
    )
    if status:
        meet_date = utils.format_date(meet_date_time.date())
        meet_time = utils.format_time(meet_date_time.time())
        response = constants.get_meeting_creation_response(meet_date, meet_time, response_value)
        return render_custom_response(response)


def set_progress_for_task(employee, result_parameters):
    task_name = result_parameters.get('Task')
    if not task_name:
        return render_custom_response("No task with this name exists.")
    completion_mark = result_parameters.get('completion_mark')
    completion_value = utils.parse_progress_dict(completion_mark)
    is_successful, custom_response = operations.set_progress_for_task(employee, task_name, completion_value)
    if not is_successful:
        response_sentence = "`{0}` doesn't exist in the list of tasks in progress under your working project.".format(task_name)
    else:
        if custom_response:
            response_sentence = custom_response
        else:
            response_sentence = "I have set the progress of `{0}` to {1}.".format(task_name, str(completion_value) + "%")
    return render_custom_response(response_sentence)


def add_project_name_to_agent(project_name):
    headers = {'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer b55df5347afe4002a39e94cd61c121c9'}
    request_dict = {"value" : project_name, "synonyms" : [project_name]}
    request_dict = json.dumps(request_dict)
    entity_request = requests.post(PROJECT_ADDITION_URL, data=request_dict, headers=headers)
    if entity_request.status_code == SUCCESS_STATUS_CODE:
        print("Entity added successfully")


def create_project_from_slack(employee, team_id, result_parameters):
    project_name = result_parameters.get('any')
    deadline_dict = result_parameters.get('deadline')
    deadline = utils.parse_deadline(deadline_dict)
    project = operations.create_project(employee, project_name, team_id, deadline)
    operations.notify_project_creation(employee, project)


def check_if_the_sender_is_manager(employee, team):
    return AccessGroup.objects.filter(group_name=ADMIN_GROUP_NAME, team=team, employees__in=[employee]).exists()


def check_if_user_is_working(result_parameters):
    date_time = result_parameters.get('date-time')
    employee_id = result_parameters.get('employee')
    employee = Employee.objects.get(unique_id=employee_id)
    is_user_working = operations.check_if_user_is_working(employee.user, date_time)
    response = utils.response_for_working_status(is_user_working, employee.user.first_name)
    return render_custom_response(response)


def delete_task(employee, result_parameters):
    task_name = result_parameters.get('task')
    status, error_message = operations.delete_task(employee, task_name)
    print(status)
    return (status, error_message)

def give_admin_access(team_id, result_parameters):
    employee_id_list = result_parameters['employee']
    operations.give_admin_access(employee_id_list, team_id)


def remove_space_in_string(string):
    return string.replace(" ", "")


def render_custom_response(sentence):
    response = {
        "speech_response": sentence
    }
    return HttpResponse(json.dumps(response),
        content_type='application/json'
    )


def handle_absence_information(employee, result_parameters, team_id):
    absence_dict = result_parameters['absence']
    if DATE_PERIOD_KEY in absence_dict.keys():
        date_range = absence_dict[DATE_PERIOD_KEY]
        for (index, character) in enumerate(date_range):
            if character == '/':
                absence_start_date = parser.parse(date_range[0 : index])
                absence_end_date = parser.parse(date_range[index + 1 :])
                absence_date = (absence_start_date, absence_end_date)
                break
    else:
        absence_date = parser.parse(absence_dict[DATE_KEY])

    operations.handle_absence(employee, absence_date, team_id)


def suggest_next_task_to_work(employee):
    task, bound, color = analytics_operations.suggest_next_task_to_work(employee)
    if bound:
        bound = utils.get_time_hours_in_readable_format(bound)
    task_name = task.task_name
    attachment = utils.generate_task_payload(
        task_name,
        employee,
        task.project.project_name,
        task.deadline,
        task.eta,
        task.completionmark.value
    )
    if not color:
        attachment['color'] = constants.GOOD_COLOR_CODE
    else:
        attachment['color'] = color
    payload = constants.get_interactive_payload(
        constants.get_next_task_suggestion(task_name, bound=bound),
        [attachment]
    )
    return utils.render_interactive_message(payload)


def change_deadline_of_task(employee, result_parameters):
    task_name = result_parameters.get('task')
    deadline_dict = result_parameters.get('deadline')
    deadline = utils.parse_deadline(deadline_dict)
    print("The new deadline")
    print(deadline)
    status, error_message = operations.change_deadline_of_task(employee, task_name, deadline)
    if status:
        utils.schedule_notification_for_nearing_deadline(employee, task_name, deadline)
    return (status, error_message)


def get_links(employee, result_parameters):
    task_name = result_parameters.get('Task')
    if task_name:
        status, response = operations.get_links(employee, task_name)
    else:
        status, response = operations.get_links(employee)
    return(status, response)


def anonymous_session_start(employee, result_parameters):
    task_name = result_parameters.get('task')
    does_task_exist = operations.check_if_task_exists_in_state_list(
        employee,
        utils.preprocess_names(task_name),
        [constants.TODO_STAGE_NAME, constants.DOING_STAGE_NAME]
    )
    if not does_task_exist:
        status = utils.create_context_for_task_creation(employee, task_name)
        return render_custom_response(
            constants.get_task_creation_flow_message(task_name)
        )
    else:
        response_string = start_work(employee, result_parameters)
        return render_custom_response(response_string)


@csrf_exempt
def handle_message(request):
    if request.method == 'GET':
        return HttpResponse("Reached server")
    if request.method == 'POST':
        recieved_json = json.loads(request.body)
        input_dict = byteify(recieved_json)
        from_node = input_dict['from_node']
        message_dict = input_dict['message']
        channel_id = message_dict['channel']
        event_type = message_dict['event']
        team_id = message_dict['team']
        user_id = message_dict['user']
        employee = Employee.objects.get(unique_id=user_id)
        message_text = input_dict['message']['text']
        open_angular_stripped = message_text.replace("<", "")
        the_stripped_message = open_angular_stripped.replace(">", "")
        print(the_stripped_message)
        team = Team.objects.get(team_id=team_id)
        message = the_stripped_message.replace("@", "")
        print(message)
        headers['Authorization'] = 'Bearer {0}'.format(CLIENT_ACCESS_TOKEN)
        print('The session id of the employee is')
        print(employee.unique_id)
        request_dict = {"query" : [message], "sessionId" : employee.unique_id, "lang" : "en" , "timezone" : "Asia/Colombo"}
        request_dict = json.dumps(request_dict)
        print('The request dict is')
        print(request_dict)
        response = requests.post(MESSAGE_SUBMISSION_URL, data=request_dict, headers=headers)
        response_dict = byteify(response.json())
        print(response_dict)
        results_dict = response_dict[RESULT_KEY]
        resolved_message = results_dict['resolvedQuery']
        intent_name = results_dict.get('metadata').get('intentName')

        result_parameters = results_dict["parameters"]
        current_context_list = []
        for context_detail in results_dict["contexts"]:
            current_context_list.append(context_detail.get("name"))
        action_incomplete = results_dict[ACTION_INCOMPLETE]
        user = User.objects.get(username=user_id)
        #generate_report(employee)
        if intent_name not in project_agnostic_intents.keys():
            channel = employee.working_project
            if channel:
                channel_name = channel.project_name
            else:
                default_project = employee.projects.filter(team=team).last()
                if default_project is None:
                    response = {
                        'speech_response' : "You are not assigned to any of the projects. Ask your manager to add you to one."
                    }
                else:
                    employee.working_project = default_project
                    employee.save()
                    response = {
                        'speech_response' : "You have started working on {0}".format(default_project.project_name)
                    }
                return HttpResponse(json.dumps(response), content_type='application/json')

        if ( intent_name == USER_ACCEPTS_DELETION_INTENT and
             CONTEXT_DELETE_TASK_CONFIRMATION in current_context_list ) :
            status, error_message = eval(results_dict['action'])(employee, result_parameters)
            if not status:
                return utils.render_custom_response(error_message)

        if ( intent_name == CREATE_TASK_DETAILS_ENTRY_INTENT and
             CREATE_TASK_FOLLOWUP_CONTEXT in current_context_list ):
            if not action_incomplete:
                status, response = eval(results_dict['action'])(employee, result_parameters)
                if constants.SESSION_START_CONTEXT in current_context_list:
                    response_string = start_work(employee, result_parameters)
                    utils.delete_session_start_context(employee, constants.SESSION_START_CONTEXT)
                    return render_custom_response(response_string)
                return response

        if intent_name == WORK_QUERY_INTENT:
            if action_incomplete == False:
                return eval(results_dict.get('action'))(result_parameters)

        if intent_name == ETA_SET_BY_USER:
            if not action_incomplete:
                status, custom_response = eval(results_dict.get('action'))(employee, result_parameters)
                if not status:
                    return utils.render_custom_response(custom_response)

        elif intent_name == CHANGE_DEADLINE_OF_TASK:
            if not action_incomplete:
                status, custom_response = eval(results_dict.get('action'))(employee, result_parameters)
                if not status:
                    message = constants.get_message_for_task_non_existence(task_name)
                    return render_custom_response(message)



        elif intent_name == GET_LINKS:
            if action_incomplete == False:
                method_call = eval(results_dict.get('action'))
                status, response_dict = method_call(employee, result_parameters)
                if status:
                    return response_dict

        elif intent_name == TASK_TIME_QUERY:
            if action_incomplete == False:
                method_call = eval(results_dict.get('action'))
                status, response_dict = method_call(employee, result_parameters)
                if status:
                    return response_dict

        elif intent_name == SCHEDULE_NEXT_TASK:
            if action_incomplete == False:
                method_call = eval(results_dict.get('action'))
                return method_call(employee)

        elif intent_name == SCHEDULE_DISCUSSION:
            if action_incomplete == False:
                method_call = eval(results_dict.get('action'))
                return method_call(employee, result_parameters)

        elif intent_name == GET_THE_CURRENT_WORKING_PROJECT:
            return operations.get_the_working_project_of_employee(employee)

        elif intent_name == CREATE_PROJECT_INTENT:
            if not action_incomplete:
                user_is_admin = check_if_the_sender_is_manager(employee, team)
                if not user_is_admin:
                    sentence = "You don't have the privilege to create a project."
                    return render_custom_response(sentence)
                method_call = eval(results_dict['action'])(employee, team_id, result_parameters)

        elif intent_name == PROVIDE_ADMIN_ACCESS_INTENT:
            user_is_admin = check_if_the_sender_is_manager(employee, team)
            if not user_is_admin:
                sentence = "You don't have this privilege."
                render_custom_response(sentence)
            else:
                method_call = eval(results_dict['action'])(team_id, result_parameters)

        elif intent_name == SET_PROGRESS_INTENT:
            return eval(results_dict['action'])(employee, result_parameters)

        elif intent_name == PROGRESS_SET_BY_USER:
            if not action_incomplete:
                return eval(results_dict['action'])(
                    employee,
                    result_parameters
                )

        elif intent_name == GET_PROGRESS_OF_TASK:
            return eval(results_dict['action'])(employee, result_parameters)

        elif intent_name == SHOW_CURRENT_TASK_IN_TIMER:
            return operations.show_current_timer(employee)

        elif intent_name == SESSION_START_INTENT_NAME:
            if action_incomplete is False:
                return eval(results_dict['action'])(request, employee, result_parameters)

        elif intent_name == RANDOM_SESSION_START:
            if action_incomplete is False:
                return eval(results_dict['action'])(employee, result_parameters)

        elif intent_name == BREAK_START_INTENT_NAME:
            return eval(results_dict['action'])(
                request,
                employee.user,
                results_dict
            )

        elif intent_name == BREAK_END_INTENT_NAME:
            method_call = eval(results_dict['action'])(request, employee.user)

        elif intent_name == SESSION_END_INTENT_NAME:
            return eval(results_dict['action'])(request, employee.user)

        elif intent_name == SET_WORK_FROM_HOME:
            if action_incomplete is False:
                absence_parameter = result_parameters['absence_parameter']
                date = parser.parse(absence_parameter['date'])

        elif intent_name == LEAVE_ABSENCE_REQUEST_INTENT:
            eval(results_dict['action'])(employee, result_parameters, team_id)
            #return HttpResponse(json.dumps(response), content_type="application/json")

        elif intent_name == LEAVE_REQUEST_ACCEPTANCE_INTENT:
            employee_name = result_parameters["employee"]
            user = User.objects.get(username=employee_name)
            recipient_email = user.email
            leave_req = user.absence_set.filter(is_approved=False).last()
            leave_req.is_approved = True
            response = "Your leave request is accepted by your manager, Have a great holiday!"
            at_stripped_email = recipient_email.replace("@", "")
            dot_stripped_email = at_stripped_email.replace(".", "")
            unique_key = "PAT{0}".format(dot_stripped_email)
            message_dict = {"senderId" : "phantom", "senderName" : "phantom", "text": response}
            db.child("master").child("channels").child(unique_key).child("messages").push(message_dict, firebase_user['idToken'])


        elif intent_name == DISCUSSION_SCHEDULE_REQUEST_INTENT:
            employee_ids = result_parameters['employee']
            task_name = result_parameters.get('Task', None)
            date = result_parameters.get('date', None)
            date_range_list = []
            employee_date_tuple_index_dict = {}
            if date:
                discussion_request_date = parser.parse(date)
            else:
                discussion_request_date = datetime.now().date()
            index = 0

            # add the id of the employee who scheduled the meeting
            employee_ids.append(employee.unique_id)
            multicast_dict = {}
            for employee_id in employee_ids:
                employee_obj = Employee.objects.get(unique_id=employee_id)
                discussion_preference = employee_obj.preference
                multicast_dict[employee_obj.slack_bot_channel_id] = None
                if discussion_preference is not None:
                    formatted_start_date_time = timezone.localtime(discussion_preference.from_date_time)
                    formatted_end_date_time = timezone.localtime(discussion_preference.to_date_time)
                    print(formatted_start_date_time)
                    if formatted_start_date_time.date() == discussion_request_date:
                        date_tuple = (formatted_start_date_time, formatted_end_date_time)
                        date_range_list.append(date_tuple)
                        employee_date_tuple_index_dict[employee_obj] = index
                        index = index + 1
            print(date_range_list)
            intersection_tuple, full_intersection = intersection_helper(request, date_range_list)
            if intersection_tuple is not None:
                start_time = intersection_tuple[0]
                start_date_time_str = str(start_time)
                request.session['intersection_tuple'] = None
                if full_intersection:
                    response = "I have scheduled your discussion at {0}".format(start_date_time_str)
                    for key in multicast_dict:
                        multicast_dict[key] = response
                    del multicast_dict[employee.slack_bot_channel_id]
                else:
                    response = "I have found an optimal time for the discussion at {0}, please schedule manually".format(start_date_time_str)
                    multicast_dict = None

            else:
                response = "I am not able to schedule an optimal discussion time for you!"
                multicast_dict = None

            print(multicast_dict)
            response = {
                "speech_response": response,
                "multicast_dict" : multicast_dict,
                "team_id" : team_id
            }
            return HttpResponse(json.dumps(response), content_type="application/json")


        elif intent_name == MEETING_INTENT_NAME:
            if action_incomplete is False:
                attendees_email_list = []
                meet_time = result_parameters['date-time']
                meet_venue = result_parameters['meet-locations']
                meet_entities = result_parameters['meetEntity']
                all_roles = Role.objects.all()
                role_names = []
                for role in all_roles:
                    role_names.append(role.role_name)
                project_obj = Project.objects.get(project_name=channel_name)
                for meet_entity in meet_entities:
                    # Belongs to a role group
                    if meet_entity in role_names:
                        selection_obj = project_obj.selections.get(role__role_name=meet_entity)
                        employees = selection_obj.employees.all()
                        for employee in employees:
                            email = employee.user.email
                            attendees_email_list.append(email)
                    # An individual meet entity
                    else:
                        email = Employee.objects.get(user__username=meet_entity).user.email
                        attendees_email_list.append(email)
                print(attendees_email_list)
                eval(results_dict['action'])(meet_time, meet_venue, attendees_email_list, user_email)

        # intent handling the user preference
        elif intent_name == SET_DISCUSSION_PREFERENCE_INTENT:

            preferred_time_interval = result_parameters['date-time']
            for (index, character) in enumerate(preferred_time_interval):
                if character == '/':
                    start_date_time = parser.parse(preferred_time_interval[0 : index])
                    end_date_time = parser.parse(preferred_time_interval[index + 1 :])
                    break
            start_hour = start_date_time.hour
            start_minute = start_date_time.minute
            formatted_start_time = time(hour=start_hour, minute=start_minute)
            start_date = start_date_time.date()
            start_date_time = datetime.combine(start_date, formatted_start_time)
            end_hour = end_date_time.hour
            end_minute = end_date_time.minute
            formatted_end_time = time(hour=end_hour, minute=end_minute)
            end_date = end_date_time.date()
            end_date_time = datetime.combine(end_date, formatted_end_time)
            eval(results_dict['action'])(employee, start_date_time, end_date_time)


        elif intent_name == GROUP_DISJUNCT_ADDITIONS:
            if action_incomplete == False:
                disjunct_names = result_parameters['any']
                task_name = result_parameters['task']
                project_obj = Project.objects.get(project_name=channel_name)
                task = Task.objects.get(task_name=task_name, project=project_obj)
                default_stage = Stage.objects.get(stage_name=TODO_STAGE_NAME)
                for disjunct_name in disjunct_names:
                    disjunct = Disjunct(disjunct_name=disjunct_name, employee=employee, task=task, stage=default_stage)
                    disjunct.save()
                add_entries_to_agent(disjunct_names)
                context_name = 'considered_task'
                session_id = employee.unique_id
                url = CONTEXTS_DELETE_ENDPOINT.format(context_name, session_id)
                print(url)
                context_delete_request = requests.delete(CONTEXTS_DELETE_ENDPOINT.format(context_name, session_id), headers=headers)
                print(context_delete_request.json())


        elif intent_name == SHIFT_TASK_REQUEST:
            default_response = utils.get_default_response(results_dict)
            return eval(results_dict['action'])(employee, result_parameters, default_response)

        elif intent_name == TASK_LIST_REQUEST:
            task_name_list_to_be_printed = []
            tasks = employee.task_set.filter(project=employee.working_project)
            if not tasks:
                response = "You don't have any task in your working project."
                response = {
                    "speech_response": response
                }
                return HttpResponse(json.dumps(response), content_type="application/json")
            the_header_string = "Your tasks are"
            task_name_list_to_be_printed.append(the_header_string)
            for (index, task) in enumerate(tasks):
                sr_no = index + 1
                the_new_list_element = str(sr_no) + '.' + " " + task.task_name
                task_name_list_to_be_printed.append(the_new_list_element)

            the_list_string = '\n'.join(task_name_list_to_be_printed)

            response = the_list_string

            response = {
                "speech_response": response
            }

            return HttpResponse(json.dumps(response), content_type="application/json")



        elif intent_name == LIST_TASKS_REQUEST_TODO:
            return eval(results_dict['action'])(employee)

        elif intent_name == LIST_TASKS_REQUEST_DOING:
            return eval(results_dict['action'])(employee)

        elif intent_name == LIST_TASKS_REQUEST_DONE:
            return eval(results_dict['action'])(employee)


        # Schedule breaks intent
        elif intent_name == SCHEDULE_BREAKS_INTENT_NAME:
            date_time_for_break = parser.parse(result_parameters['time'])
            date_time_for_break = tz.localize(date_time_for_break)
            # print(date_time_for_break)
            session = Session.objects.filter(user=employee.user).last()
            if session.end_date_time is None:
                message = "Hey, It's time for a break."
                break_message = "Please tell  if you are actually "
                channel_list = [employee.slack_bot_channel_id]
                notify_employees.apply_async(args=[message, channel_list, team_id], eta=date_time_for_break)
            else:
                response = {
                    'speech_response' : 'Sorry, You are not tracking work, Please track one and issue a break command.'
                }
                return HttpResponse(json.dumps(response), content_type='application/json')

        elif intent_name == SWITCH_PROJECT_INTENT_NAME:
            project_name = result_parameters['Project']
            project = Project.objects.get(team__team_id=team_id, project_name__iexact=project_name)
            operations.switch_project(employee, project)

        elif intent_name == ASSIGNMENT_INTENT_NAME or intent_name == ASSIGNMENT_CONTINUATION_INTENT_NAME:
            if action_incomplete is False:
                employees = result_parameters["employee"]
                print(employees)
                task_name = result_parameters["task"]
                print(task_name)
                task_obj = Task.objects.get(task_name=task_name, project__project_name=channel_name)
                project_obj = Project.objects.get(project_name=channel_name)
                for employee_name in employees:
                    if project_obj.selections.filter(employees__user__username=employee_name).exists():
                        employee_obj = Employee.objects.get(user__username=employee_name)
                        task_obj.employees.add(employee_obj)
                    else:
                        response = "The employee {0} is not a valid candidate for task assignment".format(employee_name)
                        response = {
                            "speech_response": response
                        }
                        return HttpResponse(json.dumps(response), content_type="application/json")

                deadline_dict = result_parameters.get("deadline", None)
                if deadline_dict is not None:
                    date_duration = deadline_dict.get(DATE_DEADLINE_KEY, None)
                    duration_deadline = deadline_dict.get(DURATION_DEADLINE_KEY, None)
                    if duration_deadline is not None:
                        amount = duration_deadline["amount"]
                        unit = duration_deadline["unit"]
                        if unit == "day":
                            deadline = datetime.now(tz=tz).date() + timedelta(days=int(amount))
                    else:
                        deadline = parser.parse(date_duration)
                else:
                    deadline = task_obj.deadline

                summary = task_name
                description = "Task Deadline set by channel - {0}".format(channel_name)
                default_time = datetime.now(tz=tz).time()
                datetime_obj = datetime.combine(deadline, default_time)
                datetime_obj = tz.localize(datetime_obj)
                create_event(datetime_obj, summary, description)
                task_obj.deadline = deadline
                task_obj.save()
            else:
                pass
        response = send_response(results_dict)
        return HttpResponse(json.dumps(response), content_type="application/json")


@csrf_exempt
def get_employees(request, role='WFD'):
    if request.method == 'GET':
        print(role)
        queryset = Employee.objects.filter(priority__role__role_name=role).order_by('priority__magnitude', 'user__username')
        employee_serializer = EmployeeSerializer(queryset, many=True)
        print(employee_serializer.data)
        return JsonResponse(employee_serializer.data, status=201, safe=False)


@csrf_exempt
def get_channels(request, email):
    if request.method == 'GET':
        print("Im at the getChannels API")
        employee = Employee.objects.get(user__email=email)
        projects = employee.project_set.all()
        project_serializer = ProjectSerializer(projects, many=True)
        return JsonResponse(project_serializer.data, status=200, safe=False)


@csrf_exempt
def list_links(request, channel_name):
    if request.method == 'GET':
        project = Project.objects.get(project_name=channel_name)
        queryset = project.link_set.all()
        link_serializer = LinkSerializer(queryset, many=True)
        return JsonResponse(link_serializer.data, status=200, safe=False)


@csrf_exempt
def add_employee(request):
    if request.method == 'POST':
        roles = []
        role_keys = ["1", "2", "3"]
        recieved_json = json.loads(request.body)
        recieved_dict = byteify(recieved_json)
        name = recieved_dict["name"]
        email = recieved_dict["email"]
        at_stripped_email = email.replace("@", "")
        dot_stripped_email = at_stripped_email.replace(".", "")
        input_dict = {"name": name}
        db.child("master").child(dot_stripped_email).set(input_dict, firebase_user['idToken'])
        for priorities in role_keys:
            roles.append(recieved_dict.get(priorities, None))
        print(roles)
        for(index, role) in enumerate(roles):
            if role is not None:
                role_obj = Role.objects.get(role_name=role)
                priority_obj = Priority.objects.get(role=role_obj, magnitude=index + 1)
                user = User.objects.create(username=name, email=email)
                employee_obj = Employee(user=user)
                employee_obj.save()
                employee_obj.priority.add(priority_obj)
        return HttpResponse(status=200)

@csrf_exempt
def submit_link_data(request):
    if request.method == 'POST':
        link_name = request.POST['linkName']
        selected_project = request.POST['projectList']
        url = request.POST['url']
        project = Project.objects.get(project_name=selected_project)
        link = Link(project=project, link_name=link_name, url=url)
        link.save()
        print(link)
        return HttpResponse(status=200)


@csrf_exempt
def handle_slack_interaction(request):
    if request.method == 'POST':
        recieved_json = json.loads(request.body)
        recieved_dict = byteify(recieved_json)
        actions = recieved_dict['actions']
        callback_id = recieved_dict['callback_id']
        if callback_id.startswith(constants.PROJECT_CREATION_NOTIFICATION_CALLBACK_ID):
            message = operations.handle_slack_interaction_for_project_switching(
                callback_id,
                actions
            )
            if message:
                response = {"text": message}
                return HttpResponse(
                    json.dumps(response),
                    content_type="application/json",
                    status=200
                )
        else:
            employee = Employee.objects.get(unique_id=callback_id)
            status = operations.handle_slack_interaction_for_absence(employee, actions)
        return HttpResponse(status=200)


@csrf_exempt
def handle_github_hook(request):
    if request.method == 'POST':
        user_email_list = ['sricharanprograms@gmail.com', 'sricharan14312@cse.ssn.edu.in']
        recieved_json = json.loads(request.body)
        recieved_dict = byteify(recieved_json)
        action = recieved_dict['action']
        pull_request_dict = recieved_dict['pull_request']
        user_dict = pull_request_dict['user']
        creator_login_id = user_dict['login']
        response = requests.get(GITHUB_BASE_ENDPOINT + '/users/' + creator_login_id)
        github_user_detail_dict = byteify(response.json())
        email = github_user_detail_dict.get('email', None)
        if email is None:
            creator_email = random.choice(user_email_list)
            creator_employee = Employee.objects.get(user__email=creator_email)
        else:
            creator_email = email

        github_user_detail_request_dict = {}
        assignee_list = pull_request_dict['assignees']
        for assignee_dict in assignee_list:
            login_id = assignee_dict['login']
            github_user_detail_request_dict[login_id] = None
        for login_id in github_user_detail_request_dict:
            response = requests.get(GITHUB_BASE_ENDPOINT + '/users/' + login_id)
            github_user_detail_dict = byteify(response.json())
            email = github_user_detail_request_dict.get('email', None)
            if email is None:
                email = random.choice(user_email_list)
                assigning_employee = Employee(user__email=email)
                pipe = Pipe.objects.get_or_create(mentor=assigning_employee, mentee=creator_employee)


        return HttpResponse(status=200)


def list_all_projects(request, team_id):
    if request.method == 'GET':
        team = Team.objects.get(team_id=team_id)
        queryset = team.project_set.all()
        serializer = ProjectSerializer(queryset, many=True)
        return JsonResponse(serializer.data, safe=False)


@csrf_exempt
def get_project_employees(request, channel_id):
    if request.method == 'GET':
        list_of_employees_serialized = []
        project_obj = Project.objects.get(id=int(channel_id))
        '''
        selections = project_obj.selections.all()
        for selection in selectionsg:
            queryset = selection.employees.all()
            employee_serializer = EmployeeSerializer(queryset, many=True)
            list_of_employees_serialized.append(employee_serializer.data)
        '''
        queryset = project_obj.employees.all()
        employee_serializer = EmployeeSerializer(queryset, many=True)
        return JsonResponse(employee_serializer.data, status=201, safe=False)



@csrf_exempt
def get_projects_of_employee(request, employee_id):
    if request.method == 'GET':
        employee = Employee.objects.get(id=int(employee_id))
        queryset = employee.project_set.all()
        employee_serializer = ProjectSerializer(queryset, many=True)
        return JsonResponse(employee_serializer.data, status=201, safe=False)


@csrf_exempt
def list_managers(request, team_id):
    if request.method == 'GET':
        queryset = []
        team = Team.objects.get(team_id=team_id)
        managers = Manager.objects.all()
        for manager in managers:
            teams_of_employee = manager.employee_instance.team.all()
            if team in teams_of_employee:
                queryset.append(manager)
        serializer = ManagerSerializer(queryset, many=True)
        return JsonResponse(serializer.data, safe=False)


@csrf_exempt
def get_all_employees(request, team_id):
    if request.method == 'GET':
        team = Team.objects.get(team_id=team_id)
        queryset = team.employee_set.all()
        employee_serializer = EmployeeSerializer(queryset, many=True)
        return JsonResponse(employee_serializer.data, status=201, safe=False)


# project creation api

@csrf_exempt
def create_project(request, team_id):
    if request.method == 'POST':
        recieved_json = json.loads(request.body)
        recieved_dict = byteify(recieved_json)
        # project specific details

        team = Team.objects.get(team_id=team_id)
        project_name = recieved_dict['project_name']
        manager_id = recieved_dict['manager_id']
        deadline = parser.parse(recieved_dict['deadline'])
        employee_id_list = recieved_dict['employee_id_list']

        manager_instance = Manager.objects.get(id=int(manager_id))
        project = Project(project_name=project_name, manager=manager_instance, end_date=deadline, team=team)
        project.save()
        for emp_id in employee_id_list:
            employee = Employee.objects.get(id=int(emp_id))
            project.employees.add(employee)

        headers = {'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer b55df5347afe4002a39e94cd61c121c9'}
        request_dict = {"value" : project_name, "synonyms" : [project_name]}
        request_dict = json.dumps(request_dict)
        entity_request = requests.post(PROJECT_ADDITION_URL, data=request_dict, headers=headers)
        print(entity_request.json())
        response = {
            'project_id' : project.id
        }

        return HttpResponse(json.dumps(response), content_type="application/json")

    if request.method == 'GET':
        return HttpResponse(status=403)

@csrf_exempt
def handle_queries(request):
    recieved_json = json.loads(request.body)
    recieved_dict = byteify(recieved_json)
    name = recieved_dict['name']
    email = recieved_dict['email']
    phone = recieved_dict['phone']
    company_name = recieved_dict['company_name']
    description = recieved_dict['description']
    subject = 'Query from {0}'.format(name)
    the_message_list = [description]
    contact_detail = 'Contact {0} at {1} or {2}'.format(name, phone, email)
    the_message_list.append(contact_detail)
    recipient = 'rithwinsiva@gmail.com'
    the_message_string = '\n'.join(the_message_list)
    send_mail(subject, the_message_string, DEFAULT_FROM_EMAIL, [recipient], fail_silently=False)
    return HttpResponse(status=200)


def create_users_of_slack_team(member_list, team):
    channel_id_list = []
    for member in member_list:
        unique_id = member['id']
        username = member['name']
        is_bot = member['is_bot']
        is_deleted = member['deleted']
        if is_bot or unique_id == 'USLACKBOT' or is_deleted:
            continue
        email = member['profile'].get('email')
        if not email:
            email = unique_id
        is_admin = member['is_admin']
        if not User.objects.filter(username=unique_id).exists():
            add_user_to_agent(unique_id)
            user = User.objects.create_user(username=unique_id, email=email, first_name=username)
            employee, emp_created = Employee.objects.get_or_create(
                unique_id=unique_id,
                user=user,
                defaults={'unique_id': unique_id, 'user': user}
            )
            employee.save()
            team.employee_set.add(employee)
        else:
            user = User.objects.get(username=unique_id)
            employee, emp_created = Employee.objects.get_or_create(
                unique_id=unique_id,
                user=user,
                defaults={'unique_id': unique_id, 'user': user}
            )
            if team not in employee.team.all():
                team.employee_set.add(employee)
        team.save()
        if is_admin:
            admin_group = AccessGroup.objects.create(group_name=ADMIN_GROUP_NAME, team=team)
            admin_group.save()
            admin_group.employees.add(employee)
        channel_id = operations.get_channel_from_user_id(team.access_token, unique_id)
        if channel_id:
            employee.slack_bot_channel_id = channel_id
            employee.save()
            channel_id_list.append(channel_id)
    return channel_id_list


# Handling oauth requests from Slack
@csrf_exempt
def handle_oauth_flow(request):
    auth_code = request.GET.get('code')
    if not auth_code:
        return HttpResponse(status=400)
    exchange_url = 'https://slack.com/api/oauth.access?client_id={0}&client_secret={1}&code={2}'.format(CLIENT_ID, CLIENT_SECRET, auth_code)
    oauth_exchange_request = requests.get(exchange_url)
    the_dict = oauth_exchange_request.json()
    bot_dict = the_dict.get('bot', None)
    team_name = the_dict.get('team_name', None)
    team_id = the_dict.get('team_id', None)
    access_token  = the_dict.get('access_token')
    if not access_token:
        return HttpResponse(status=400)
    # This is a add team to slack request
    if bot_dict is not None:
        bot_access_token = bot_dict.get('bot_access_token', None)
        team, created = Team.objects.get_or_create(
            team_id=team_id,
            defaults={'access_token': bot_access_token, 'team_name': team_name, 'team_id': team_id}
        )
        if created:
            users_detail = requests.get('https://slack.com/api/users.list?token={0}'.format(access_token))
            users_detail = byteify(users_detail.json())
            member_list = users_detail['members']
            channel_id_list = create_users_of_slack_team(member_list, team)
            notify_new_team.delay(team_id, bot_access_token, channel_id_list)
        return HttpResponseRedirect("https://www.phantomk8.com/slackthanks.html")

    # Slack sign in
    else:
        user_id = the_dict['user_id']
        try:
            employee = Employee.objects.get(
                unique_id=user_id,
            )
            api_key = employee.user.api_key.key
            response_dict = {"api_key": api_key}
        except Employee.DoesNotExist:
            response_dict = {
                constants.DOES_NOT_EXIST_ERROR_CODE: "Employee Does not exist"
            }
        return HttpResponse(
            json.dumps(response_dict),
            content_type='application/json'
        )


@csrf_exempt
def assign_tasks_gui(request, team_id):
    recieved_json = json.loads(request.body)
    print(recieved_json)
    recieved_dict = byteify(recieved_json)

    project_id = recieved_dict['project_id']
    task_name = recieved_dict['task_name']
    deadline = parser.parse(recieved_dict['deadline'])
    employees_id_list = recieved_dict['employee_id_list']

    project = Project.objects.get(id=int(project_id))
    stage, created = Stage.objects.get_or_create(stage_name=TODO_STAGE_NAME)
    stage.save()
    task = Task(task_name=task_name, project=project, deadline=deadline, stage=stage)
    task.save()
    channel_list = []
    task_team = []
    for emp_id in employees_id_list:
        employee = Employee.objects.get(id=int(emp_id))
        task_team.append(employee.user.first_name)
        channel_list.append(employee.slack_bot_channel_id)
        task.employees.add(employee)

    # Add task to agent

    headers = {'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer b55df5347afe4002a39e94cd61c121c9'}
    request_dict = {"value" : task.task_name, "synonyms" : [task.task_name]}
    request_dict = json.dumps(request_dict)
    entity_request = requests.post(TASK_ENTITY_ADDITION_URL, data=request_dict, headers=headers)
    print(entity_request.json())


    message_to_agent = "create disjuncts for {0}".format(task.task_name)
    print(message_to_agent)

    # add context(the newly created task) to all employees assigned to the task
    context_list = []
    context_dict = {"name": "considered_task", "lifespan": 5, "parameters": {"task" : task.task_name} }
    context_list.append(context_dict)
    headers = {'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer b55df5347afe4002a39e94cd61c121c9'}
    request_dict= json.dumps(context_list)
    for employee in task.employees.all():
        context_request = requests.post(CONTEXTS_ENDPOINT.format(employee.unique_id), data=request_dict, headers=headers)
        print(context_request.json())


    #send_automated_message_to_nlp(message_to_agent, task.employees.all())

    base_message = "Hey, you are assigned with a new task"
    task_description = "TASK: {0}, DEADLINE: {1} ".format(task.task_name, str(task.deadline.strftime('%Y-%m-%d')))
    project_detail = "PROJECT: {0}".format(project.project_name)
    task_team_string = "The task team: "
    create_disjunct_prompt = "Please provide your disjuncts for this task as a set of comma separated values. Example: disjunct_1, disjunct_2, disjunct_3."
    count = len(task_team)
    for index, name in enumerate(task_team):
        if index == count - 1:
            task_team_string = task_team_string + name + "."
        else:
            task_team_string = task_team_string + name + ", "


    message_list = [base_message, task_description, project_detail, task_team_string]
    the_message_string = '\n'.join(message_list)
    the_message_string = the_message_string + '\n' + create_disjunct_prompt

    notify_employees.delay(the_message_string, channel_list, team_id)

    response = {
        'task_id' : task.id
    }
    return HttpResponse(json.dumps(response), content_type="application/json")


def handle_gui_signin(request):
    oauth_initiation_request = requests.get(OAUTH_SLACK_INITIATION_URL)


def developer_stage_instantiation(request):
    stage_doing = Stage(id=2, stage_name=DOING_STAGE_NAME)
    stage_doing.save()
    stage_done = Stage(id=3, stage_name=DONE_STAGE_NAME)
    stage_done.save()
    return HttpResponse(Stage.objects.all())

def handle_manager_view(request, team_id, channel_id):
    project = Project.objects.get(id=int(channel_id))
    queryset = project.task_set.all()
    task_serializer = TaskSerializer(queryset, many=True)
    return JsonResponse(task_serializer.data, status=201, safe=False)


def get_access_tokens(request):
    queryset = Team.objects.all()
    team_serializer = TeamSerializer(queryset, many=True)
    return JsonResponse(team_serializer.data, status=201, safe=False)


@csrf_exempt
def render_home(request):
    return render(request, 'PGE/api_welcome_page.html')

@csrf_exempt
def handle_slash_command(request):
    print("I am HttpResponseRedirect")
    response = "It's 80 degrees right now."
    return HttpResponse(json.dumps(response))

