from datetime import datetime
from datetime import timedelta
from dateutil import parser

from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.http import HttpApplicationError
from tastypie.http import HttpBadRequest
from tastypie.http import HttpNotFound
from tastypie.http import HttpUnauthorized
from tastypie.resources import ModelResource
from tastypie.utils import trailing_slash

from django.conf.urls import include
from django.conf.urls import url

from PGE import analytics_operations
from PGE import utils
from PGE.models import Employee
from PGE.models import Project
from PGE.models import Task


class ProjectResource(ModelResource):
    employees = fields.ToManyField(
        'PGE.employee_api.EmployeeResource',
        'working_employees',
        full=True
    )

    class Meta:
        queryset = Project.objects.all()
        resource_name ='project'
        excludes = ['end_date', 'slack_channel_id']
        include_resource_uri = False

    def dehydrate_start_date(self, bundle):
        start_date = bundle.data['start_date']
        return start_date.strftime('%d/%m/%Y')

    def get_object_list(self, request):
        is_user_authentic, user = utils.is_user_authentic(request)
        if not is_user_authentic:
            return self.error_response(request,
                utils.get_error_response(
                'UnAuthorized',
                'User not allowed to access the resource'),
                HttpUnauthorized)
        manager_id = user.employee.id
        return super(ProjectResource, self).get_object_list(request).filter(managers__employee_instance__id=manager_id)

    def alter_list_data_to_serialize(self, request, data_dict):
        if isinstance(data_dict, dict):
            if 'meta' in data_dict:
                # Get rid of the "meta".
                del(data_dict['meta'])

        return data_dict

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/get/productivity-score%s$" %
            (self._meta.resource_name, trailing_slash()),
            self.wrap_view('get_project_productivity_score'),
            name="api_get_project_productivity_score"),
        ]

    def get_project_productivity_score(self, request, **kwargs):
        is_user_authentic, user = utils.is_user_authentic(request)
        if not is_user_authentic:
            return self.error_response(request,
                utils.get_error_response(
                'UnAuthorized',
                'User not allowed to access the resource'),
                HttpUnauthorized
            )
        request_parameters = utils.decode_json(request.body)
        project_id = request_parameters.get('project_id')
        start_date = parser.parse(request_parameters.get('start_date'))
        end_date = parser.parse(request_parameters.get('end_date'))
        end_date = datetime.combine(
            end_date.date(),
            datetime.now().time().max
        )
        project = Project.objects.get(id=int(project_id))
        response_dict = analytics_operations.get_productivity_score(
            project_id,
            start_date,
            end_date
        )
        return self.create_response(request, response_dict)
