import json
import pytz
import re
import requests

from datetime import datetime
from datetime import timedelta
from dateutil import parser
from tastypie.models import ApiKey

from django.contrib.auth import login
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.utils import timezone

from PGE import constants
from PGE import tasks


def try_converting_to_int(number):
    try:
        return (True, int(number))
    except ValueError:
        return (False, None)


# Can be made asynchronous
def add_name_to_agent(name, url, synonym):
    print(name)
    print(synonym)
    request_dict = {"value" : name, "synonyms" : [synonym]}
    serialized_dict = json.dumps(request_dict)
    entity_request = requests.post(url,
        data=serialized_dict,
        headers=constants.HEADER_FOR_AGENT_MANIPULATION
    )
    print(entity_request.status_code)
    if entity_request.status_code == constants.SUCCESS_STATUS_CODE:
        print(entity_request.json())
        return True
    else:
        return False


def preprocess_names(string):
    return re.sub(constants.STRING_FOR_SPACES, '.', string=string)


def prepare_channel_list_for_notification(employees):
    channel_list = []
    for employee in employees:
        channel_list.append(employee.slack_bot_channel_id)
    return channel_list


def prepare_and_notify_employees(employees, team_id, message, is_message_interactive=False):
    channel_list = prepare_channel_list_for_notification(employees)
    tasks.notify_employees.delay(message, channel_list, team_id, is_message_interactive)


def prepare_and_notify_employees_at_specific_time(employees, team_id, message, time_to_invoke):
    channel_list = prepare_channel_list_for_notification(employees)
    tasks.notify_employees.apply_async(
        args=[message, channel_list, team_id],
        eta=time_to_invoke
    )


def days_hours_minutes(td):
    return td.days, td.seconds//3600, (td.seconds//60)%60


def format_date_hours_minutes_worked(days, hours, minutes):
    response_string = ""
    is_component_present = False
    if days != 0:
        if days > 1:
            response_string = response_string + "{0} days".format(days)
        else:
            response_string = response_string + "{0} day".format(days)
        is_component_present = True

    if hours != 0:
        if is_component_present:
            response_string = response_string + ", "
        if hours > 1:
            response_string = response_string + "{0} hours".format(hours)
        else:
            response_string = response_string + "{0} hour".format(hours)
        is_component_present = True

    if minutes != 0:
        if is_component_present:
            response_string = response_string + " " + "and "

        if minutes > 1:
            response_string = response_string + "{0} minutes".format(minutes)
        else:
            response_string = response_string + "{0} minute".format(minutes)

    return response_string


def format_end_session_response(days, hours, minutes, task_name):
    if hours == 0 and minutes == 0:
        response = "You haven't worked for a minute."
    else:
        work_time_string = format_date_hours_minutes_worked(days, hours, minutes)
        response = "Ending work session, you've worked on `{0}` for {1}.".format(task_name, work_time_string)
    return response


def render_custom_response(sentence):
    response = {
        "speech_response": sentence
    }
    return HttpResponse(json.dumps(response),
        content_type='application/json'
    )


def render_interactive_message(payload):
    response = {
        'payload': payload
    }
    return HttpResponse(
        json.dumps(response),
        content_type='application/json'
    )


def response_for_working_status(is_user_working, username):
    if is_user_working:
        response = "Yes, {} is working.".format(username)
    else:
        response = "No, {} is not working.".format(username)
    return response


def render_interactive_request_for_slack(text, title, callback_id):
    text = text
    actions = [
        {
            "name": "Yes",
            "text": "Yes",
            "type": "button",
            "value": constants.ABSENCE_ACCEPTANCE_VALUE
        },
        {
            "name": "No",
            "text": "No",
            "type": "button",
            "value": constants.ABSENCE_REJECTION_VALUE
        }

    ]
    title = title
    callback_id = callback_id
    attachment_type = 'default'
    inner_response_dict = {'title':title, 'callback_id':callback_id, 'attachment_type':attachment_type, 'actions':actions}
    outer_response_dict = {'text': text, 'attachments':[inner_response_dict]}
    return outer_response_dict


def get_default_response(results_dict):
    fulfillment = results_dict['fulfillment']
    speech_response = fulfillment['speech']
    return speech_response


def schedule_notification_for_nearing_deadline(employee, task_name, deadline):
    team_id = employee.working_project.team.team_id
    trigger_time = deadline - timedelta(hours=1)
    if trigger_time > datetime.now(tz=constants.tz):
        message = constants.message_for_deadline_approach(task_name)
        prepare_and_notify_employees_at_specific_time([employee], team_id, message, trigger_time)


def format_time(time):
    return time.strftime("%I:%M %p")


def format_date(date):
    return date.strftime("%d/%m/%Y")


def decode_json(str_data):
    try:
        return json.loads(str_data)
    except (ValueError, TypeError):
        raise ValueError


def convert_to_UTC(datetime):
    if datetime.tzinfo:
        return datetime
    else:
        return datetime.astimezone(pytz.UTC)


def parse_deadline(deadline_dict):
    date_duration = deadline_dict.get("date_deadline")
    time_duration = deadline_dict.get("time_deadline")
    duration_deadline = deadline_dict.get("duration_deadline")
    if duration_deadline is not None:
        amount = duration_deadline["amount"]
        unit = duration_deadline["unit"]
        if unit == "day":
            deadline = datetime.now(tz=constants.tz) + timedelta(days=int(amount))
        elif unit == "h":
            deadline = datetime.now(tz=constants.tz) + timedelta(hours=int(amount))
        else:
            deadline = datetime.now(tz=constants.tz) + timedelta(minutes=int(amount))
    elif date_duration:
        deadline = constants.tz.localize(parser.parse(date_duration))
    else:
        deadline = constants.tz.localize(parser.parse(time_duration))
    return deadline


def get_deadline_in_hours(duration_dict):
    duration_deadline = duration_dict.get("duration_deadline")
    if duration_deadline is not None:
        amount = duration_deadline["amount"]
        unit = duration_deadline["unit"]
        if unit == "day":
            hours = int(amount) * 24
        elif unit == "h":
            hours = int(amount)
        else:
            hours = int(amount) / 60.0
    return hours


def convert_timedelta_to_hours(interval):
    return interval.seconds / 3600.0


def create_context(employee, request_dict):
    context_request = requests.post(
        constants.CONTEXTS_ENDPOINT.format(employee.unique_id),
        data=request_dict,
        headers=constants.HEADER_FOR_AGENT_MANIPULATION
    )
    if context_request.status_code == constants.SUCCESS_STATUS_CODE:
        return True
    else:
        return False


def delete_session_start_context(employee, context_name):
    url = constants.SPECIFIC_CONTEXT_MODIFICATION_ENDPOINT.format(context_name, employee.unique_id)
    response = requests.delete(url, headers=constants.HEADER_FOR_AGENT_MANIPULATION)
    print(response.status_code)


def create_context_for_eta_updation(employee, task_name):
    context_dict = {"name": "set_eta", "lifespan": 5, "parameters": {"task": task_name}}
    request_dict = json.dumps(context_dict)
    status = create_context(employee, request_dict)
    return status


def create_context_for_progress_updation(employee, task_name):
    context_dict = {"name": "set_progress", "lifespan": 5, "parameters": {"task": task_name}}
    request_dict = json.dumps(context_dict)
    status = create_context(employee, request_dict)
    return status


def create_context_for_task_creation(employee, task_name):
    context_dict = {"name": "session_start", "lifespan": 5, "parameters": {"task": task_name}}
    request_dict = json.dumps(context_dict)
    status = create_context(employee, request_dict)
    return status


def format_user_id(unique_id):
    return "<@{0}>".format(unique_id)


def format_url(url):
    return "<{0}>".format(url)


def get_user_ids(employee_list):
    formatted_user_id_list = []
    for employee in employee_list:
        user_id = format_user_id(employee.unique_id)
        formatted_user_id_list.append(user_id)
    return formatted_user_id_list


def is_user_authentic(request):
    api_token = request.META.get('HTTP_AUTHORIZATION', request.META.get('HTTP_X_AUTHORIZATION', None))
    if api_token is None:
        return (False, None)
    try:
        key_obj = ApiKey.objects.get(key=api_token)
        user = key_obj.user
        login(request, user)
        return (True, request.user)
    except ApiKey.DoesNotExist:
        return (False, None)


def generate_token_for_user(user):
    key_obj, created = ApiKey.objects.get_or_create(user=user, defaults={'user': user})
    return key_obj.key


def get_error_response(errorCode, errorMessage):
    return {
        constants.API_ERROR_CODE: errorCode,
        constants.API_ERROR_MESSAGE: errorMessage
    }


def format_date_and_time(datetime):
    return datetime.strftime("%d/%m/%Y %I:%M %p")


def compute_time_in_sessions(sessions, required_result_format=None):
    session_timedelta = timedelta(0, 0, 0)
    for session in sessions:
        if session.end_date_time:
            session_timedelta = (session_timedelta +
                (
                    session.end_date_time - session.start_date_time
                )
            )
        else:
            session_timedelta = (session_timedelta +
                (
                    datetime.now(constants.tz) - timezone.localtime(session.start_date_time)
                )
            )
    if required_result_format == constants.HOURS:
        return convert_timedelta_to_hours(session_timedelta)
    days, hours, minutes = days_hours_minutes(session_timedelta)
    return (days, hours, minutes)


def get_time_hours_in_readable_format(time_in_hours):
    print(time_in_hours)
    days = int(time_in_hours // 24)
    hours = int((time_in_hours % 24) // 1)
    minutes = int((time_in_hours % 1) * 60)
    response_string = format_date_hours_minutes_worked(days, hours, minutes)
    return response_string


def generate_task_payload(task_name, employee, project_name, deadline, eta_in_hours, completion_value):
    eta_string = get_time_hours_in_readable_format(eta_in_hours)
    deadline_string = format_date_and_time(timezone.localtime(deadline))
    employee_name_string = format_user_id(employee.unique_id)
    attachment = constants.get_task_detail_display_attachment(
        constants.get_task_description_header(),
        task_name,
        project_name,
        employee_name_string,
        deadline_string,
        eta_string,
        completion_value
    )
    return attachment


def is_user_a_staff(user):
    return user.is_staff


def parse_progress_dict(completion_mark):
    if isinstance(completion_mark, dict):
        completion_mark = completion_mark.get('mark_in_percent')
    did_conversion_happen, number = try_converting_to_int(completion_mark.replace("%", ""))
    if not did_conversion_happen:
        return
    else:
        completion_value = number
    return completion_value


def get_project_created_message(unique_id, project_name):
    return "{0} created project `{1}`.".format(format_user_id(unique_id), project_name)
