from __future__ import unicode_literals

import pytz

from django.contrib.auth.models import User
from django.db import models
from datetime import datetime, timedelta
from django.utils import timezone

from datetime import datetime

from PGE import constants


class Role(models.Model):

    WEB_FRONT_DEVELOPER = "WFD"
    BACKEND_DEVELOPER = "BD"
    ANDROID_DEVELOPER = "AD"
    iOS_DEVELOPER = "ID"
    DESIGNER = "DS"

    ROLE_CHOICES = (
        (WEB_FRONT_DEVELOPER, "WebFrontendDeveloper"),
        (BACKEND_DEVELOPER, "BackendDeveloper"),
        (ANDROID_DEVELOPER, "AndroidDeveloper"),
        (iOS_DEVELOPER, "iOSDeveloper"),
        (DESIGNER, "Designer"),
    )

    role_name = models.CharField(choices=ROLE_CHOICES, max_length=3, default=ANDROID_DEVELOPER, unique=True)

    def __str__(self):
        return self.role_name


class Priority(models.Model):
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    magnitude = models.IntegerField()


class Team(models.Model):

    FREE_TRIAL= "FT"
    BASIC_PLAN = "BP"

    PLAN_CHOICES = (
        (FREE_TRIAL, "FREE_TRIAL"),
        (BASIC_PLAN, "BASIC_PLAN")
    )

    team_id = models.CharField(max_length=100, unique=True, default=None)
    team_name = models.CharField(max_length=100)
    access_token = models.CharField(max_length=250)
    joined_at = models.DateTimeField(default=timezone.now)
    plan = models.CharField(choices=PLAN_CHOICES, default=FREE_TRIAL, max_length=4)

    def __str__(self):
        return self.team_name


class Project(models.Model):
    project_name = models.CharField(max_length=100, default=None)
    start_date = models.DateField(default=datetime.now)
    end_date = models.DateField(default=None, null=True)
    deadline = models.DateTimeField(default=None, null=True)
    #selections = models.ManyToManyField(Selection)
    slack_channel_id = models.CharField(max_length=50, default=None, null=True)
    team = models.ForeignKey(Team, null=True, default=None)

    def __str__(self):
        return self.project_name


class ProjectLog(models.Model):
    project = models.OneToOneField(Project, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    finished_at = models.DateTimeField(default=None, null=True)


class Employee(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, default=None)
    priority = models.ManyToManyField(Priority)
    unique_id = models.CharField(max_length=20, default=None, null=True)
    slack_bot_channel_id = models.CharField(max_length=20, default=None, null=True)
    team = models.ManyToManyField(Team)
    working_project = models.ForeignKey(Project, related_name="working_employees", default=None, null=True)
    projects = models.ManyToManyField(Project, related_name="assigned_employees")

    def __str__(self):
        return self.user.email


class AccessGroup(models.Model):
    group_name = models.CharField(max_length=20)
    team = models.OneToOneField(Team, on_delete=models.CASCADE, default=None)
    employees = models.ManyToManyField(Employee)


class Selection(models.Model):
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    employees = models.ManyToManyField(Employee)


class Manager(models.Model):
    employee_instance = models.OneToOneField(Employee)
    projects = models.ManyToManyField(Project, related_name='managers')

    def __str__(self):
        return self.employee_instance.user.email


class Stage(models.Model):
    TODO = "101"
    DOING = "102"
    DONE = "103"

    STAGE_CHOICES = (
        (TODO, "TODO"),
        (DOING, "DOING"),
        (DONE, "DONE")
    )

    stage_name = models.CharField(max_length=10, choices=STAGE_CHOICES, default=TODO, unique=True)

    def __str__(self):
        return self.stage_name


class Task(models.Model):
    task_name = models.CharField(max_length=100, default=None)
    start_date = models.DateField(default=datetime.now)
    deadline = models.DateTimeField(default=None, null=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, default=None, null=True)
    employees = models.ManyToManyField(Employee)
    stage = models.ForeignKey(Stage, default=None, null=True)
    # The estimated completion time of the task in hours.
    eta = models.FloatField(default=None, null=True)

    def set_eta(self, eta):
        self.eta = eta
        self.save()

    def get_eta(self):
        return self.eta

    def __str__(self):
        return self.task_name


class TaskLog(models.Model):
    task = models.OneToOneField(Task, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    finished_at = models.DateTimeField(default=None, null=True)
    last_modified_at = models.DateTimeField(auto_now_add=True)
    progress_updated_at = models.DateTimeField(default=timezone.now)


class CompletionMark(models.Model):
    value = models.IntegerField()
    task = models.OneToOneField(Task, on_delete=models.CASCADE)



class Discussion(models.Model):
    organizer = models.ForeignKey(Employee, default=None, related_name='discussions_organized')
    participants = models.ManyToManyField(Employee, related_name='discussions')
    start_date_time = models.DateTimeField(default=None, null=True)
    approximate_end_date_time = models.DateTimeField(default=None, null=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, default=None)

    def __str__(self):
        return "A discussion is scheduled"


class Disjunct(models.Model):
    disjunct_name = models.CharField(max_length=100, default=None)
    task = models.ForeignKey(Task, on_delete=models.CASCADE, default=None)
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    stage = models.ForeignKey(Stage)

    def __str__(self):
        return self.disjunct_name


class Pipe(models.Model):
    mentor = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name="mentor", default=None)
    mentee = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name="mentee", default=None)
    disjuncts = models.ManyToManyField(Disjunct)


class Preference(models.Model):
    employee = models.OneToOneField(Employee, on_delete=models.CASCADE)
    from_date_time = models.DateTimeField(default=None, null=True)
    to_date_time = models.DateTimeField(default=None, null=True)

    def __str__(self):
        return "A new discussion preference is set for the employee"


class Session(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    start_date_time = models.DateTimeField(default=datetime.now)
    end_date_time = models.DateTimeField(default=None, null=True)
    disjunct = models.ForeignKey(Disjunct, on_delete=models.CASCADE, default=None, null=True)
    # Experiment to timetrack tasks instead of disjuncts
    task = models.ForeignKey(Task, on_delete=models.CASCADE, default=None, null=True)
    is_master_session = models.BooleanField(default=False)

    def __str__(self):
        return self.user.email


class Absence(models.Model):

    WORK_FROM_HOME = "WFH"
    LEAVE = "LV"

    ABSENCE_TYPES = (
        (WORK_FROM_HOME, "Work from home"),
        (LEAVE, "Take a leave"),
    )

    employee = models.ForeignKey(Employee, on_delete=models.CASCADE, default=None)
    start_date_time = models.DateTimeField(default=None)
    end_date_time = models.DateTimeField(default=None)
    absence_type = models.CharField(choices=ABSENCE_TYPES, max_length=3, default=LEAVE)
    is_approved = models.BooleanField(default=False)


    def __str__(self):
        return self.employee.user.email

class MessageLog(models.Model):
    message = models.TextField()
    intent = models.CharField(max_length=100)


class Link(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE, default=None, null=True)
    name = models.CharField(max_length=200, default=None)
    url = models.URLField(max_length=500)
    added_by = models.ForeignKey(Employee, on_delete=models.CASCADE, default=None, related_name="added_link")

    def __str__(self):
        return self.name


class Lead(models.Model):
    PROSPECT = "PR"

    LEAD_STAGES = (
        (PROSPECT, "PROSPECT"),
    )

    name = models.CharField(max_length=20)
    email = models.EmailField(max_length=50)
    company = models.CharField(max_length=20)
    phone_number = models.CharField(max_length=15)
    stage = models.CharField(choices=LEAD_STAGES, max_length=3, default=PROSPECT)

    def __str__(self):
        return self.company
