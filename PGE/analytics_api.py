from datetime import datetime
from dateutil import parser

from tastypie.authorization import Authorization
from tastypie.http import HttpBadRequest
from tastypie.http import HttpNotFound
from tastypie.http import HttpUnauthorized
from tastypie.resources import ModelResource
from tastypie.utils import trailing_slash

from django.conf.urls import include
from django.conf.urls import url

from PGE import analytics_operations
from PGE import constants
from PGE import operations
from PGE import utils
from PGE.models import Project
from PGE.models import Session
from PGE.models import User


class AnalyticsResource(ModelResource):
    class Meta:
        queryset = Session.objects.all()
        resource_name = 'analytics/chart'
        authorization = Authorization()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/progress%s$" %
            (self._meta.resource_name, trailing_slash()),
            self.wrap_view('project_progress'),
            name="api_project_progress"),
            url(r"^(?P<resource_name>%s)/session%s$" %
            (self._meta.resource_name, trailing_slash()),
            self.wrap_view('get_session_chart'),
            name="api_get_session_chart"),
            url(r"^(?P<resource_name>%s)/momentum%s$" %
            (self._meta.resource_name, trailing_slash()),
            self.wrap_view('get_momentum_chart'),
            name="api_get_momentum_chart"),
            url(r"^(?P<resource_name>%s)/work-time-pie%s$" %
            (self._meta.resource_name, trailing_slash()),
            self.wrap_view('get_work_time_pie_chart'),
            name="api_get_work_time_pie_chart"),
        ]

    def get_work_time_pie_chart(self, request, **kwargs):
        is_user_authentic, user = utils.is_user_authentic(request)
        if not is_user_authentic:
            return self.error_response(request,
                utils.get_error_response(
                'UnAuthorized',
                'User not allowed to access the resource'),
                HttpUnauthorized
            )
        request_parameters = utils.decode_json(request.body)
        project_id = request_parameters.get('project_id')
        response = analytics_operations.get_work_time_pie_chart_details(project_id)
        return self.create_response(request, response)

    def get_session_chart(self, request, **kwargs):
        is_user_authentic, user = utils.is_user_authentic(request)
        if not is_user_authentic:
            return self.error_response(request,
                utils.get_error_response(
                'UnAuthorized',
                'User not allowed to access the resource'),
                HttpUnauthorized
            )
        request_parameters = utils.decode_json(request.body)
        project_id = request_parameters.get('project_id')
        employee_id = request_parameters.get('employee_id')
        start_date = parser.parse(request_parameters.get('start_date'))
        end_date = parser.parse(request_parameters.get('end_date'))
        sessions = analytics_operations.get_session_chart_details(
            project_id,
            employee_id,
            start_date,
            end_date
        )
        return self.create_response(request, list(sessions))

    def get_momentum_chart(self, request, **kwargs):
        is_user_authentic, user = utils.is_user_authentic(request)
        if not is_user_authentic:
            return self.error_response(request,
                utils.get_error_response(
                'UnAuthorized',
                'User not allowed to access the resource'),
                HttpUnauthorized
            )
        request_parameters = utils.decode_json(request.body)
        project_id = request_parameters.get('project_id')
        start_date = parser.parse(request_parameters.get('start_date'))
        end_date = parser.parse(request_parameters.get('end_date'))
        end_date = datetime.combine(
            end_date.date(),
            datetime.now().time().max
        )
        project = Project.objects.get(id=int(project_id))
        response_dict = analytics_operations.get_momentum_chart_details(
            project_id,
            start_date,
            end_date
        )
        return self.create_response(request, response_dict)


    def project_progress(self, request, **kwargs):
        is_user_authentic, user = utils.is_user_authentic(request)
        if not is_user_authentic:
            return self.error_response(request,
                utils.get_error_response(
                'UnAuthorized',
                'User not allowed to access the resource'),
                HttpUnauthorized
            )
        request_parameters = utils.decode_json(request.body)
        project_id = request_parameters.get('project_id')
        from_date_time = parser.parse(request_parameters.get('start_date'))
        to_date_time = parser.parse(request_parameters.get('end_date'))
        project = Project.objects.get(id=int(project_id))
        to_date_time = datetime.combine(
            to_date_time.date(),
            datetime.now().time().max
        )
        project_progress_dict = analytics_operations.project_progress(
            project,
            from_date_time,
            to_date_time
        )
        return self.create_response(request, project_progress_dict)
