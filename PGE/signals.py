from datetime import datetime
from datetime import timedelta

from django.contrib.auth.models import User
from django.db.models.signals import post_delete
from django.db.models.signals import post_save

from PGE.models import CompletionMark
from PGE.models import Employee
from PGE.models import Project
from PGE.models import ProjectLog
from PGE.models import Session
from PGE.models import Task
from PGE.models import TaskLog
from PGE.models import Team

from PGE import constants
from PGE import operations
from PGE import utils


def create_log_on_task_creation(sender, instance, created, **kwargs):
    if created:
        TaskLog.objects.create(task=instance)


def set_zero_progress_on_task_creation(sender, instance, created, **kwargs):
    if created:
         CompletionMark.objects.create(task=instance, value=0)


def update_log_to_mark_task_completion(sender, instance, created, **kwargs):
    if instance.stage.stage_name == constants.DONE_STAGE_NAME:
        task_log = TaskLog.objects.get(task=instance)
        task_log.finished_at = datetime.now(constants.tz)
        task_log.save()


def update_last_modified_time_in_task_log(sender, instance, created, **kwargs):
    if created:
        task = instance.task
        task_log = TaskLog.objects.get(task=task)
        task_log.last_modified_at = datetime.now(constants.tz)
        task_log.save()


def check_if_progress_update_is_needed(sender, instance, created, **kwargs):
    if not created and instance.end_date_time:
        operations.check_if_progress_update_is_needed(instance)


def create_log_on_project_creation(sender, instance, created, **kwargs):
    if created:
        ProjectLog.objects.create(project=instance)


def generate_api_key_for_user(sender, instance, created, **kwargs):
    if created:
        utils.generate_token_for_user(instance)


def delete_user_instance_on_associated_employee_deletion(sender, instance, *args, **kwargs):
    try:
        user = User.objects.get(employee=instance)
        user.delete()
    except User.DoesNotExist:
        pass

def delete_team_employees(sender, instance, *args, **kwargs):
    Employee.objects.filter(team=instance).delete()


def update_log_to_reflect_progress_updation(sender, instance, *args, **kwargs):
    task = instance.task
    tl = TaskLog.objects.get(task=task)
    tl.progress_updated_at = datetime.now(constants.tz)


# Provide connections for signals
post_delete.connect(delete_team_employees, sender=Team)
post_delete.connect(delete_user_instance_on_associated_employee_deletion, sender=Employee)
post_save.connect(create_log_on_task_creation, sender=Task)
post_save.connect(update_log_to_mark_task_completion, sender=Task)
post_save.connect(set_zero_progress_on_task_creation, sender=Task)
post_save.connect(update_log_to_reflect_progress_updation, sender=CompletionMark)
post_save.connect(update_last_modified_time_in_task_log, sender=Session)
post_save.connect(check_if_progress_update_is_needed, sender=Session)
post_save.connect(create_log_on_project_creation, sender=Project)
post_save.connect(generate_api_key_for_user, sender=User)

