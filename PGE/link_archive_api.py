from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.http import HttpApplicationError
from tastypie.http import HttpBadRequest
from tastypie.http import HttpNotFound
from tastypie.http import HttpUnauthorized
from tastypie.resources import ModelResource
from tastypie.utils import trailing_slash

from django.conf.urls import include
from django.conf.urls import url
from django.http import HttpResponse

from PGE.models import Employee
from PGE.models import Link
from PGE.models import Project
from PGE.models import Task
from PGE import utils


class LinkResource(ModelResource):
    class Meta:
        queryset = Link.objects.all()
        resource_name = 'link'
        include_resource_uri = False

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/add%s$" %
            (self._meta.resource_name, trailing_slash()),
            self.wrap_view('add_link'),
            name="api_add_link"),
        ]

    def add_link(self, request, **kwargs):
        is_user_authentic, user = utils.is_user_authentic(request)
        if not is_user_authentic:
            return self.error_response(request,
                utils.get_error_response(
                'UnAuthorized',
                'User not allowed to access the resource'),
                HttpUnauthorized
            )
        request_parameters = utils.decode_json(request.body)
        print(request_parameters)
        name = request_parameters.get("name")
        link = request_parameters.get("link")
        project_id = request_parameters.get("project_id")
        task_id = request_parameters.get("task_id")
        project = Project.objects.get(id=int(project_id))
        link = Link(
            name=name,
            url=link,
            project=project,
            added_by=user.employee
        )
        if task_id:
            task = Task.objects.get(id=int(task_id))
            link.task = task
        link.save()
        return self.create_response(request, {'status': 'OK'})

