# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-01-08 05:24
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('PGE', '0047_auto_20171006_2106'),
    ]

    operations = [
        migrations.AddField(
            model_name='session',
            name='task',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='PGE.Task'),
        ),
        migrations.AlterField(
            model_name='project',
            name='start_date',
            field=models.DateField(default=datetime.date(2018, 1, 8)),
        ),
        migrations.AlterField(
            model_name='session',
            name='disjunct',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='PGE.Disjunct'),
        ),
        migrations.AlterField(
            model_name='session',
            name='start_date_time',
            field=models.DateTimeField(default=datetime.datetime(2018, 1, 8, 10, 54, 58, 209126)),
        ),
        migrations.AlterField(
            model_name='task',
            name='project',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='PGE.Project'),
        ),
        migrations.AlterField(
            model_name='task',
            name='start_date',
            field=models.DateField(default=datetime.date(2018, 1, 8)),
        ),
    ]
