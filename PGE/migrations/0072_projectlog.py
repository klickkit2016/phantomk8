# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2018-03-03 06:26
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('PGE', '0071_auto_20180220_1926'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('finished_at', models.DateTimeField(default=None, null=True)),
                ('project', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='PGE.Project')),
            ],
        ),
    ]
