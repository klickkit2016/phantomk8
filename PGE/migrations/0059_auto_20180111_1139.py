# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-01-11 06:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('PGE', '0058_auto_20180110_1443'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stage',
            name='stage_name',
            field=models.CharField(choices=[('101', 'TODO'), ('102', 'DOING'), ('103', 'DONE')], default='101', max_length=10, unique=True),
        ),
    ]
