from django.apps import apps
from django.contrib import admin

from PGE.models import *

for model in apps.get_app_config('PGE').models.values():
    admin.site.register(model)
