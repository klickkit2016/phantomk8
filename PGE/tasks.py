import json
import os
import requests

from celery.decorators import periodic_task
from celery.decorators import task
from celery.schedules import crontab

from django.conf import settings
from django.core.mail.message import EmailMessage

from PGE import constants
from PGE import operations
from PhantomGabEngine.settings import DEFAULT_FROM_EMAIL


NOTIFICATION_SERVER_HOST = constants.PRODUCTION_BOT_SERVER
INTERACTIVE_MESSAGE_URL = '/interactive/message/send'
PRIVATE_MESSAGE_URL = '/private/message/send'
TEXT_MESSAGE_URL = "/notify/channels"


@task(name="notify employees")
def notify_employees(message, channel_list, team_id, is_message_interactive=False):
    if is_message_interactive:
        url = NOTIFICATION_SERVER_HOST + INTERACTIVE_MESSAGE_URL
    else:
        url = NOTIFICATION_SERVER_HOST + TEXT_MESSAGE_URL

    request = {
        'message' : message,
        'channel_list' : channel_list,
        'team_id' : team_id
    }
    request_json = json.dumps(request)
    print(request_json)
    headers = {'Content-Type': 'application/json'}
    multicast_request = requests.post(url, data=request_json, headers=headers)
    return


@task(name="custom message notification")
def notify_employees_individually(team_id, channel_and_message_dict):
    url = NOTIFICATION_SERVER_HOST + PRIVATE_MESSAGE_URL
    request = {
        'team_id': team_id,
        'channel_and_message': channel_and_message_dict
    }
    request_json = json.dumps(request)
    headers = {'Content-Type': 'application/json'}
    multicast_request = requests.post(url, data=request_json, headers=headers)
    return


@task(name="notify new team")
def notify_new_team(team_id, access_token, channel_id_list):
    url = NOTIFICATION_SERVER_HOST + "/notify/new/team"
    request = {
        'team_id': team_id,
        'access_token': access_token,
        'channel_id_list': channel_id_list
    }
    request_json = json.dumps(request)
    headers = {'Content-Type': 'application/json'}
    multicast_request = requests.post(url, data=request_json, headers=headers)
    return


@task(name="send emails")
def send_email(subject, body, recipient_list, attachment_file_name=None):
    email = EmailMessage()
    email.subject = subject
    email.body = body
    email.from_email = DEFAULT_FROM_EMAIL
    email.to = recipient_list
    if attachment_file_name:
        email.attach_file(attachment_file_name)
        file = open(attachment_file_name, "rb")
        email.attach(filename = attachment_file_name, mimetype = "text/csv", content = file.read())
        file.close()
    email.send()


@task(name="check_if_the_task_is_completed")
def check_if_task_is_completed(*args):
    task_id = args[0]
    employee_id = args[1]
    is_task_completed = operations.check_if_task_in_state(task_id, constants.DONE_STAGE_NAME)
    if not is_task_completed:
        operations.handle_deadline_skipping(task_id, employee_id)


@periodic_task(run_every=crontab(minute='*/10'))
def check_draining_of_eta():
    operations.check_draining_of_eta()
