
from PGE import constants
from PGE import utils


class ETAManager:

    def update_eta_of_task(self, task, duration):
        duration = utils.convert_timedelta_to_hours(duration)
        new_eta = task.eta - duration
        if new_eta > 0:
            task.eta = new_eta
            task.set_eta(new_eta)
            return (True, None)
        elif new_eta < 0:
            return (False, constants.NEGATIVE_ETA_REMAINING)
        else:
            return (False, constants.ZERO_ETA_REMAINING)

    def set_new_eta_for_task(self, task, time_in_hours):
        task.set_eta(time_in_hours)
        return True

    def get_eta_of_task(self, task):
        eta_in_hours = task.get_eta()
        response_string = utils.get_time_hours_in_readable_format()
        return response_string
