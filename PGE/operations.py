import json
import pytz
import re
import requests

from datetime import datetime
from datetime import timedelta
from dateutil import parser

from django.contrib.auth import login
from django.contrib.auth import logout
from django.db.models import F
from django.db.models import Q
from django.utils import timezone

from PGE.models import AccessGroup
from PGE.models import CompletionMark
from PGE.models import Discussion
from PGE.models import Employee
from PGE.models import Manager
from PGE.models import MessageLog
from PGE.models import Link
from PGE.models import Project
from PGE.models import Session
from PGE.models import Stage
from PGE.models import Task
from PGE.models import TaskLog
from PGE.models import Team

from PGE import constants
from PGE.ETAManager import ETAManager
from PGE import tasks
from PGE import utils

tz = pytz.timezone('Asia/Kolkata')

def create_task(employee, task_name, deadline, eta):
    processed_task_name = utils.preprocess_names(task_name)
    working_project = employee.working_project
    stage, stage_created = Stage.objects.get_or_create(stage_name=constants.TODO_STAGE_NAME,
        defaults={'stage_name': constants.TODO_STAGE_NAME}
    )
    task, created = Task.objects.get_or_create(
        task_name__iexact=processed_task_name,
        project=working_project,
        defaults={'task_name': processed_task_name, 'project': working_project, 'stage': stage}
    )
    task.stage = stage
    task.deadline = deadline
    task.eta = eta
    task.employees.add(employee)
    task.save()
    if created:
        utils.add_name_to_agent(processed_task_name, constants.ENTITY_ADDITION_URL.format("Task"), task_name)
    attachment = utils.generate_task_payload(
        task_name,
        employee,
        task.project.project_name,
        deadline,
        eta,
        task.completionmark.value
    )
    payload = constants.get_interactive_payload(
        constants.get_new_task_added_header(),
        [attachment]
    )
    return(task, utils.render_interactive_message(payload))


def create_project(employee, project_name, team_id, deadline):
    team = Team.objects.get(team_id=team_id)
    processed_project_name = utils.preprocess_names(project_name)
    project, project_created = Project.objects.get_or_create(
        project_name__iexact=processed_project_name, team=team,
        defaults={'project_name': processed_project_name, 'team': team}
    )
    manager, created = Manager.objects.get_or_create(
        employee_instance=employee,
        defaults={'employee_instance': employee}
    )
    project.managers.add(manager)
    project.deadline = deadline
    project.save()
    if project_created:
        utils.add_name_to_agent(
            processed_project_name,
            constants.ENTITY_ADDITION_URL.format("Project"),
            project_name
        )
    return project


def add_to_message_logs(message, intent):
    MessageLog.objects.create(message=message, intent=intent)


def give_admin_access(employee_id_list, team_id):
    admin_group = AccessGroup.objects.get(group_name=constants.ADMIN_GROUP_NAME, team__team_id=team_id)
    employees = []
    for emp_id in employee_id_list:
        employees.append(Employee.objects.get(unique_id=emp_id))
    admin_group.employees.add(*employees)
    admin_notification_message = "You are now an admin."
    utils.prepare_and_notify_employees(employees, team_id, admin_notification_message)


def filter_tasks_in_state(employee, state_list):
    return employee.task_set.filter(
        project=employee.working_project,
        stage__stage_name__in=state_list
    ).order_by('-tasklog__last_modified_at')


def check_if_task_exists_in_state_list(employee, task_name, state_list):
    return Task.objects.filter(
        employees=employee,
        task_name=task_name,
        stage__stage_name__in=state_list
    ).exists()


def update_eta_of_task(session):
    employee = session.user.employee
    team_id = session.task.project.team.team_id
    duration = session.end_date_time - session.start_date_time
    task = session.task
    status, error_code = ETAManager().update_eta_of_task(task, duration)
    if not status:
        context_addition_status = utils.create_context_for_eta_updation(employee, task.task_name)
        message = constants.get_eta_drain_warning_message(task)
        utils.prepare_and_notify_employees(
            [employee],
            team_id,
            message
        )


def move_task_to_state_and_return_task(employee, task_name, stage_name, allowed_states):
    task = employee.task_set.get(
        task_name__iexact=task_name,
        project=employee.working_project,
        stage__stage_name__in=allowed_states
    )
    stage, created = Stage.objects.get_or_create(stage_name=stage_name,
        defaults={'stage_name': stage_name}
    )
    task.stage = stage
    task.save()

    return task


def create_new_session(user, task, if_end_break=False):
    closed_a_running_session = False
    user_last_session = Session.objects.filter(user=user).last()
    if user_last_session is not None and user_last_session.end_date_time is None:
        user_last_session.end_date_time = datetime.now(tz=tz)
        closed_a_running_session = True
        switch_response = end_work(user)
    session = Session.objects.create(user=user, task=task, start_date_time=datetime.now(tz=constants.tz))
    if if_end_break is False:
        session.is_master_session = True
    session.save()
    # Update the log to reflect last modified time
    if closed_a_running_session:
        return (constants.SWITCHED_NEW_SESSION, switch_response)
    else:
        return (constants.REGULAR_NEW_SESSION, None)


def close_session(user):
    try:
        session = Session.objects.get(end_date_time=None, user=user)
        session.end_date_time = datetime.now(tz=constants.tz)
        session.save()
        return True, None

    except Session.DoesNotExist:
        return False, constants.get_not_working_message()


def resume_work(request, user):
    session = Session.objects.filter(user=user, is_master_session=True).last()
    create_new_session(user, session.task, if_end_break=True)


def end_work(user):
    close_session(user)
    master_session = Session.objects.filter(user=user, is_master_session=True).last()
    sessions_in_scope = Session.objects.filter(user=user, id__gte=master_session.id)
    days, hours, minutes = utils.compute_time_in_sessions(sessions_in_scope)
    task_name = master_session.task.task_name
    response = utils.format_end_session_response(days, hours, minutes, task_name)
    return response


def set_progress_for_task(employee, task_name, completion_value):
    task = employee.task_set.filter(
        task_name__iexact=task_name,
        project=employee.working_project,
        stage__stage_name=constants.DOING_STAGE_NAME
    ).first()
    if task is None:
        return (False, None)
    if completion_value == 100:
        shift_status, end_session_response = shift_tasks(employee, [task_name])
        return (True, end_session_response)
    completion_mark_instance, created = CompletionMark.objects.get_or_create(task=task, defaults={'task': task,
        'value': completion_value})
    completion_mark_instance.value = completion_value
    completion_mark_instance.save()
    return (True, None)


def shift_tasks(employee, task_names):
    for task_name in task_names:
        task_instance = Task.objects.get(
            task_name__iexact=task_name,
            project=employee.working_project
        )
        is_there_a_running_session_with_task = check_if_there_is_a_running_session(task_instance, employee)
        next_stage_name = str(int(task_instance.stage.stage_name) + 1)
        if next_stage_name > constants.DONE_STAGE_NAME:
            return (False, constants.response_for_task_already_completed_error(task_name))
        stage, created = Stage.objects.get_or_create(stage_name=next_stage_name,
            defaults={'stage_name': next_stage_name}
        )
        task_instance.stage = stage
        if next_stage_name == constants.DONE_STAGE_NAME:
            task_log = TaskLog.objects.get(task=task_instance)
            task_log.finished_at = datetime.now(constants.tz)
            task_log.save()
        task_instance.save()
    if is_there_a_running_session_with_task:
        end_session_response = end_work(employee.user)
        return(True, end_session_response)
    else:
        return(True, None)


def check_if_user_is_working(user, date_time):
    is_date_time_in_range = False
    for (index, character) in enumerate(date_time):
        if character == '/':
            is_date_time_in_range = True
            query_start_date_time = utils.convert_to_UTC(parser.parse(date_time[0 : index]))
            query_end_date_time = utils.convert_to_UTC(parser.parse(date_time[index + 1 :]))
            break
    if not is_date_time_in_range:
        date_time = utils.convert_to_UTC(parser.parse(date_time))
        print(date_time)
        does_session_exist = Session.objects.filter(
            Q(end_date_time__gte=date_time) |
            Q(end_date_time=None),
            user=user).exists()
    else:
        minimum_start_time_for_sessions = datetime.combine(query_start_date_time.date(),
            datetime.now().time().min)
        does_session_exist = Session.objects.filter(
            start_date_time__gte=minimum_start_time_for_sessions,
            user=user).exclude(
            start_date_time__gt=query_end_date_time,
            end_date_time__lt=query_start_date_time
        ).exists()
    return does_session_exist


def check_if_there_is_a_running_session(task, employee):
    return Session.objects.filter(
        task=task,
        end_date_time=None
    ).exists()


def handle_absence(employee, absence_date, team_id):
    if isinstance(absence_date, tuple):
        absence_start_date = absence_date[0].strftime('%d/%m/%Y')
        absence_end_date = absence_date[1].strftime('%d/%m/%Y')
        message_for_managers = "{} has requested for absence from {} to {}.".format(
            employee.user.first_name,
            absence_start_date,
            absence_end_date
        )
    else:
        absence_date = absence_date.strftime('%d/%m/%Y')
        message_for_managers = "{} has requested for absence on {}.".format(
            employee.user.first_name,
            absence_date
        )
    managers = employee.working_project.managers.select_related('employee_instance')
    employees_to_be_notified = []
    for manager in managers:
        employees_to_be_notified.append(manager.employee_instance)

    title = constants.title_for_interactive_message_for_absence_request()
    interactive_message_dict = utils.render_interactive_request_for_slack(
        message_for_managers,
        title,
        employee.unique_id
    )
    utils.prepare_and_notify_employees(
        employees_to_be_notified,
        team_id,
        interactive_message_dict,
        is_message_interactive=True
    )


def show_current_timer(employee):
    try:
        session = Session.objects.get(user=employee.user, end_date_time=None)
        task_name = session.task.task_name
        work_time = datetime.now(tz=tz) - session.start_date_time
        days, hours, minutes = utils.days_hours_minutes(work_time)
        work_time_string = utils.format_date_hours_minutes_worked(days, hours, minutes)
        sentence = constants.work_time_display(task_name, work_time_string)
        return utils.render_custom_response(sentence)
    except Session.DoesNotExist:
        sentence = "Sorry, you are not tracking work right now."
        return utils.render_custom_response(sentence)


def get_the_working_project_of_employee(employee):
    response = constants.response_for_currently_working_project(employee.working_project.project_name)
    return utils.render_custom_response(response)


def handle_slack_interaction_for_absence(employee, actions):
    team_id = employee.working_project.team.team_id
    absence_is_approved = False
    for action_dict in actions:
        if action_dict['value'] == constants.ABSENCE_ACCEPTANCE_VALUE:
            absence_is_approved = True
        else:
            pass
    message = constants.get_absence_response_based_on_acceptance(absence_is_approved)
    utils.prepare_and_notify_employees(
        [employee],
        team_id,
        message,
    )
    return True


def get_progress_of_task(employee, task_name):
    task_instance = Task.objects.filter(
        task_name__iexact=task_name,
        project=employee.working_project,
        stage__stage_name=constants.DOING_STAGE_NAME
    ).first()

    if task_instance is None:
        return (False, constants.QUERY_NO_MATCH_MESSAGE)

    try:
        completion_mark = CompletionMark.objects.get(task=task_instance)
        completion_value = completion_mark.value
        return (True, completion_value)

    except CompletionMark.DoesNotExist:
        return (False, None)


def schedule_discussion(organizer, employee_ids, meet_start_time, meet_end_time):
    custom_notification_dict = {}
    project = organizer.working_project
    participants = Employee.objects.filter(unique_id__in=employee_ids)
    discussion = Discussion.objects.create(
        start_date_time=meet_start_time,
        approximate_end_date_time=meet_end_time,
        project=project,
        organizer=organizer
    )
    discussion.participants.add(*participants)
    discussion.save()
    meet_id = constants.get_meeting_id(project.project_name, discussion.id)
    participants = list(participants)
    employees_to_notify = participants + [organizer]
    user_ids_list = utils.get_user_ids(employees_to_notify)
    meet_start_date_in_readable_format = utils.format_date(meet_start_time.date())
    meet_start_time_in_readable_format = utils.format_time(meet_start_time.time())
    for user_id, employee in zip(user_ids_list, employees_to_notify):
        custom_message = constants.get_message_for_meeting(list(set(user_ids_list) - {user_id}))
        custom_message = custom_message + "on {} at {}.".format(meet_start_date_in_readable_format, meet_start_time_in_readable_format)
        custom_notification_dict[employee.slack_bot_channel_id] = custom_message
    notification_time = meet_start_time - timedelta(minutes=15)
    team_id = organizer.working_project.team.team_id
    tasks.notify_employees_individually.delay(team_id, custom_notification_dict)
    if notification_time > datetime.now(tz=constants.tz):
        tasks.notify_employees_individually.apply_async(
            args=[team_id, custom_notification_dict],
            eta=notification_time
        )
    return (True, meet_id)


def set_new_eta_for_task(employee, task_name, eta_in_hours):
    try:
        task = Task.objects.get(
            task_name__iexact=task_name,
            project=employee.working_project,
            stage__stage_name=constants.DOING_STAGE_NAME
        )
        ETAManager().set_new_eta_for_task(task, eta_in_hours)
        return (True, None)
    except Task.DoesNotExist:
        return (False, constants.DOES_NOT_EXIST_ERROR_CODE)


def get_managers_of_employee_in_working_project(employee):
    return employee.working_project.managers.select_related('employee_instance')


def check_if_task_in_state(task_id, stage_name):
    try:
        task = Task.objects.get(
            id=task_id,
            stage__stage_name=stage_name
        )
        return True
    except Task.DoesNotExist:
        return False


def handle_deadline_skipping(task_id, employee_id):
    employee = Employee.objects.get(id=employee_id)
    employees_to_notify = [manager.employee_instance
        for manager in get_managers_of_employee_in_working_project(employee)
    ]
    channel_list = utils.prepare_channel_list_for_notification(employees_to_notify)
    team_id = employee.working_project.team.team_id
    task = Task.objects.get(id=task_id)
    task_name = task.task_name
    project_name = task.project.project_name
    attachment = constants.get_deadline_skipped_attachment(
        task_name,
        project_name,
        utils.format_user_id(employee.unique_id)
    )
    payload = constants.get_interactive_payload("", [attachment])
    tasks.notify_employees.delay(
        payload,
        channel_list,
        team_id,
        is_message_interactive=True
    )


def schedule_reminder_for_task_completion_check(task_id, employee_id, deadline):
    tasks.check_if_task_is_completed.apply_async(
        args=[task_id, employee_id],
        eta=deadline
    )


def delete_task(employee, task_name):
    try:
        employee.task_set.get(task_name__iexact=task_name).delete()
        return (True, None)
    except Task.DoesNotExist:
        return (False, constants.get_message_for_task_non_existence(task_name))


def get_task_time(employee, task_name):
    try:
        task = employee.task_set.get(task_name__iexact=task_name)
        attachment = utils.generate_task_payload(
            task_name,
            employee,
            task.project.project_name,
            task.deadline,
            task.eta,
            task.completionmark.value
        )
        sessions = Session.objects.filter(task=task)
        hours = utils.compute_time_in_sessions(
            sessions,
            required_result_format=constants.HOURS
        )
        time = utils.get_time_hours_in_readable_format(hours)
        payload = constants.get_interactive_payload(
            constants.display_time_of_work(time),
            [attachment]
        )
        return(True, utils.render_interactive_message(payload))
    except Task.DoesNotExist:
        return(False, constants.DOES_NOT_EXIST_ERROR_CODE)


def check_draining_of_eta():
    sessions = Session.objects.filter(end_date_time=None).select_related('task')
    for session in sessions:
        duration_of_work = datetime.now(tz=pytz.utc) - session.start_date_time
        duration_in_hours = utils.convert_timedelta_to_hours(duration_of_work)
        team_id = session.task.project.team.team_id
        employee = session.user.employee
        if (session.task.eta - duration_in_hours) <= 0:
            session.end_date_time = datetime.now(tz=constants.tz)
            session.save()
            context_addition_status = utils.create_context_for_eta_updation(
                employee,
                session.task.task_name
            )
            message = "Closing session! "
            message = message + constants.get_eta_drain_warning_message(session.task)
            utils.prepare_and_notify_employees(
                [employee],
                team_id,
                message
            )


def change_deadline_of_task(employee, task_name, deadline):
    try:
        task = Task.objects.get(
            task_name__iexact=task_name,
            project=employee.working_project,
        )
        task.deadine = deadline
        task.save()
        return (True, None)
    except Task.DoesNotExist:
        return (False, constants.DOES_NOT_EXIST_ERROR_CODE)


def check_if_progress_update_is_needed(session):
    task = session.task
    number_of_sessions = task.session_set.filter(
        start_date_time__gte=task.tasklog.progress_updated_at
    ).count()
    if number_of_sessions >= constants.SESSIONS_BEFORE_NEXT_UPDATE:
        status = utils.create_context_for_progress_updation(
            session.user.employee,
            task.task_name
        )
        employee = session.user.employee
        team_id = employee.working_project.team.team_id
        message = constants.get_progress_prompt_message(session.task.task_name)
        utils.prepare_and_notify_employees([employee], team_id, message)


def delete_team(team_id):
    try:
        Team.objects.get(team_id=team_id).delete()
        return True
    except Team.DoesNotExist:
        return False


def get_links(employee, task_name=None):
    if task_name:
        links = Link.objects.filter(
            added_by=employee,
            task__task_name__iexact=task_name
        ).values('name', 'url', by=F('added_by__unique_id'))
        title = "Links of task `{0}`.".format(task_name)
    else:
        links = Link.objects.filter(
            project=employee.working_project
        ).values('name', 'url', by=F('added_by__unique_id'))
        title = "Links of project `{0}`.".format(employee.working_project.project_name)
    fields_list = []
    for link_detail in list(links):
        field_dict = {}
        value_string = utils.format_url(link_detail["url"]) + '\n' + utils.format_user_id(link_detail["by"])
        field_dict["title"] = link_detail["name"]
        field_dict["value"] = value_string
        field_dict["short"] = False
        fields_list.append(field_dict)
    attachment = constants.get_links_detail_attachment("", fields_list)
    payload = constants.get_interactive_payload(
        title,
        [attachment]
    )
    return(True, utils.render_interactive_message(payload))


def get_channel_from_user_id(token, unique_id):
    response_parameters = requests.get(constants.get_im_endpoint(token, unique_id)).json()
    if response_parameters.get('ok'):
        return response_parameters['channel']['id']
    else:
        return None


def notify_project_creation(employee, project):
    callback_id = constants.get_project_creation_notification_callback_id(employee.id, project.id)
    attachment = constants.get_project_creation_payload(callback_id)
    payload = constants.get_interactive_payload(
        utils.get_project_created_message(
            employee.unique_id,
            project.project_name
        ),
        [attachment]
    )
    team = project.team
    employees = team.employee_set.all()
    utils.prepare_and_notify_employees(
        employees,
        team.team_id,
        payload,
        is_message_interactive=True
    )


def switch_project(employee, project):
    project.assigned_employees.add(employee)
    project.save()
    employee.working_project = project
    employee.save()


def handle_slack_interaction_for_project_switching(callback_id, actions):
    pattern = re.compile(constants.PATTERN_STRING_FOR_MATCHING_INTEGERS)
    matches = re.findall(pattern, callback_id)
    employee_id = matches[0]
    project_id = matches[1]
    option = actions[0]
    if option["value"] == "switch_project_yes":
        employee = Employee.objects.get(id=int(employee_id))
        project = Project.objects.get(id=int(project_id))
        switch_project(employee, project)
        return constants.get_project_switch_message(project.project_name)
    else:
        return None

def send_cheatsheet():
    attachment = constants.get_cheetsheet_attachment()
    payload = constants.get_interactive_payload(
        "",
        [attachment]
    )


def create_task_and_start_work(employee, task_name):
    processed_task_name = utils.preprocess_names(task_name)
    if check_if_task_exists_in_state_list(
        employee,
        processed_task_name,
        [constants.TODO_STAGE_NAME, constants.DOING_STAGE_NAME]
    ):
        pass
    else:
        status = utils.create_context_for_task_creation(employee, processed_task_name)
        print("Task creation context status")
        print(status)

