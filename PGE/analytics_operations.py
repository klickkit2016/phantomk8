import math

from datetime import datetime
from datetime import timedelta

from django.db.models import F
from django.db.models import Q
from django.utils import timezone

from PGE.models import Employee
from PGE.models import Project
from PGE.models import ProjectLog
from PGE.models import Session
from PGE.models import Task

from PGE import constants
from PGE import operations
from PGE import utils


def completed_and_remaining_tasks_of_day(tasks, date, end_date, day_wise_details_dict):
    if date > end_date:
        pass
    else:
        end_of_the_day = datetime.combine(date.date(), datetime.now().time().max)
        tasks_for_tomorrow = tasks.all().exclude(
            tasklog__finished_at__gte=date,
            tasklog__finished_at__lte=end_of_the_day
        )
        remaining_tasks_for_the_day = tasks_for_tomorrow.filter(
            tasklog__created_at__lt=end_of_the_day
        )
        day_wise_details_dict[str(date.date())] = {
            'tasks_completed': len(tasks) - len(tasks_for_tomorrow),
            'tasks_remaining': len(remaining_tasks_for_the_day)
        }
        tomorrow = date + timedelta(days=1)
        day_wise_details_dict = completed_and_remaining_tasks_of_day(
            tasks_for_tomorrow,
            tomorrow,
            end_date,
            day_wise_details_dict
        )
    return day_wise_details_dict


def completed_tasks_for_each_day(tasks, date, end_date, day_wise_details_dict):
    if date > end_date:
        pass
    else:
        end_of_the_day = datetime.combine(date.date(), datetime.now().time().max)
        tasks_for_tomorrow = tasks.all().exclude(
            tasklog__finished_at__gte=date,
            tasklog__finished_at__lte=end_of_the_day
        )
        day_wise_details_dict[str(date.date())] = len(tasks) - len(tasks_for_tomorrow)
        tomorrow = date + timedelta(days=1)
        day_wise_details_dict = completed_tasks_for_each_day(
            tasks_for_tomorrow,
            tomorrow,
            end_date,
            day_wise_details_dict
        )
    return day_wise_details_dict


def get_tasks_in_period(project, from_date_time, to_date_time):
    tasks = project.task_set.all().exclude(
        Q(tasklog__finished_at__lt=from_date_time) |
        Q(tasklog__created_at__gt=to_date_time)
    )
    return tasks


def get_tasks_of_employee_in_period(project, employee, from_date_time, to_date_time):
    tasks = employee.task_set.filter(project=project).exclude(
        Q(tasklog__finished_at__lt=from_date_time) |
        Q(tasklog__created_at__gt=to_date_time),
    )
    return tasks


def project_progress(project, from_date_time, to_date_time):
    tasks = get_tasks_in_period(project, from_date_time, to_date_time)
    data_dict = completed_and_remaining_tasks_of_day(
        tasks,
        from_date_time,
        to_date_time,
        {}
    )
    return data_dict


def get_session_chart_details(project_id, employee_id, start_date_time, end_date_time):
    return (Session.objects.filter(
        user__employee__id=int(employee_id),
        task__project__id=int(project_id),
        start_date_time__gte=start_date_time,
    ).exclude(start_date_time__gte=end_date_time
    ).select_related('task').annotate(task_name=F('task__task_name')).values
    (
        'id',
        'start_date_time',
        'end_date_time',
        'task_name'
    ))


def get_slope(number_of_tasks, duration_in_days):
    slope = (number_of_tasks / duration_in_days)
    return slope


def get_momentum_chart_details(project_id, start_date, end_date):
    project = Project.objects.get(id=int(project_id))
    tasks = get_tasks_in_period(project, start_date, end_date)
    data_dict = completed_tasks_for_each_day(
        tasks,
        start_date,
        end_date,
        {}
    )
    duration = (end_date - start_date).days
    slope = get_slope(tasks.count(), duration)
    response_dict = {'slope': slope, 'completion_magnitude': data_dict}
    return response_dict


def get_productivity_score(project_id, start_date, end_date):
    project = Project.objects.get(id=int(project_id))
    tasks = get_tasks_in_period(project, start_date, end_date)
    tasks_that_are_done_count = tasks.filter(
        stage__stage_name=constants.DONE_STAGE_NAME
    ).count()
    try:
        score = tasks_that_are_done_count / tasks.count()
        return {'score': math.ceil(score * 100)}
    except ZeroDivisionError:
        return {'score': 0}


def get_work_time_pie_chart_details(project_id):
    try:
        project = Project.objects.get(id=int(project_id))
        project_created_at = timezone.localtime(
            ProjectLog.objects.get(project=project).created_at
        )
        today = datetime.now(constants.tz)
        expected_working_hours = ((today - project_created_at).days) * 8
        sessions = Session.objects.filter(task__project=project)
        actual_working_hours = utils.compute_time_in_sessions(
            sessions,
            required_result_format=constants.HOURS
        )
        response_dict = {
            'expected_working_hours': expected_working_hours,
            'actual_working_hours': actual_working_hours
        }
        return response_dict
    except Project.DoesNotExist:
        pass


def generate_task_table(project_id, employee_id, start_date_time, end_date_time):
    project = Project.objects.get(id=int(project_id))
    employee = Employee.objects.get(id=int(employee_id))
    tasks = get_tasks_of_employee_in_period(
        project,
        employee,
        start_date_time,
        end_date_time
    ).prefetch_related('session_set')
    task_list = []
    for task in list(tasks):
        task_name = task.task_name
        intuitive_stage_name = constants.STAGE_MAPPING.get(task.stage.stage_name)
        sessions = task.session_set.filter(
            start_date_time__gte=start_date_time,
            start_date_time__lte=end_date_time
        )
        days, hours, minutes = utils.compute_time_in_sessions(sessions)
        work_time = utils.format_date_hours_minutes_worked(days, hours, minutes)
        number_of_sessions = len(sessions)
        task_list.append({
            "task_name": task_name,
            "status": intuitive_stage_name,
            "work_time": work_time,
            "number_of_sessions": number_of_sessions
        })
    return task_list


def suggest_next_task_to_work(employee):
    tasks = operations.filter_tasks_in_state(
        employee,
        [constants.DOING_STAGE_NAME, constants.TODO_STAGE_NAME]
    ).prefetch_related('session_set').select_related('completionmark').order_by('deadline')
    dp = [None for i in range(len(tasks))]
    print(tasks)
    for index, task in enumerate(list(tasks)):
        sessions = task.session_set.all()
        hours = utils.compute_time_in_sessions(
            sessions,
            required_result_format=constants.HOURS
        )
        completed_percent = task.completionmark.value
        current_time = datetime.now(constants.tz)
        deadline_in_local_time = timezone.localtime(task.deadline)
        if deadline_in_local_time < current_time:
            return (task, None, constants.DANGER_COLOR_CODE)
        rate = hours / completed_percent
        remaining_hours = utils.convert_timedelta_to_hours(
            deadline_in_local_time - current_time
        )
        needed_hours = (constants.HUNDRED_PERCENT - completed_percent) * rate
        tag = (needed_hours - remaining_hours)
        print("The tag is")
        print(tag)
        if tag < 0:
            if index == 0 or tag > dp[index - 1][0]:
                dp[index] = (tag, task, index)
            else:
                dp[index] = dp[index - 1]
        else:
            if index == 0:
                return (task, None, constants.WARNING_COLOR_CODE)
            else:
                return (task, abs(dp[index - 1][0]), constants.WARNING_COLOR_CODE)
        print(dp)
    least_negative_tuple = dp[index]
    if least_negative_tuple[2] != 0:
        return (least_negative_tuple[1], abs(dp[least_negative_tuple[2] - 1][0]))
    else:
        return (least_negative_tuple[1], None)
