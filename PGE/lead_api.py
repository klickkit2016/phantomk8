from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.http import HttpApplicationError
from tastypie.http import HttpBadRequest
from tastypie.http import HttpNotFound
from tastypie.http import HttpUnauthorized
from tastypie.resources import ModelResource
from tastypie.utils import trailing_slash

from django.conf.urls import include
from django.conf.urls import url
from django.http import HttpResponse
from django.http import HttpResponseRedirect

from PGE import utils
from PGE.models import Employee
from PGE.models import Lead
from PGE.models import Project
from PGE.models import Task


class LeadResource(ModelResource):

    class Meta:
        queryset = Lead.objects.all()
        resource_name = 'lead'
        include_resource_uri = False

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/add/prospect%s$" %
            (self._meta.resource_name, trailing_slash()),
            self.wrap_view('add_prospect'),
            name="api_add_prospect"),
        ]

    def add_prospect(self, request, **kwargs):
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        if first_name and last_name:
            name = first_name + '\n' + last_name
        elif first_name:
            name = first_name
        else:
            name = last_name
        email = request.POST.get('email')
        phone_number = request.POST.get('phoneno')
        company = request.POST.get('company')
        Lead.objects.create(
            name=name,
            email=email,
            company=company,
            phone_number=str(phone_number)
        )
        return HttpResponseRedirect("https://www.phantomk8.com/signupthanks.html")
