from datetime import datetime
from dateutil import parser

from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.http import HttpBadRequest
from tastypie.http import HttpNotFound
from tastypie.http import HttpUnauthorized
from tastypie.resources import ModelResource
from tastypie.utils import trailing_slash

from django.conf.urls import include
from django.conf.urls import url
from django.http import HttpResponse

from PGE import analytics_operations
from PGE import constants
from PGE import operations
from PGE import utils
from PGE.models import Employee
from PGE.models import Project
from PGE.models import Session
from PGE.models import User


class EmployeeResource(ModelResource):
    user = fields.ToOneField(
        'PGE.account_api.UserResource',
        'user',
        full=True
    )
    class Meta:
        queryset = Employee.objects.all()
        resource_name = 'employee'
        excludes = ['priority', 'unique_id', 'slack_bot_channel_id']
        include_resource_uri = False
        authorization = Authorization()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/get/task-table%s$" %
            (self._meta.resource_name, trailing_slash()),
            self.wrap_view('get_task_table'),
            name="api_get_task_table"),
            url(r"^(?P<resource_name>%s)/set/slack-channel-id%s$" %
            (self._meta.resource_name, trailing_slash()),
            self.wrap_view('set_slack_channel_id'),
            name="api_set_slack_channel_id"),
        ]

    def get_task_table(self, request, **kwargs):
        is_user_authentic, user = utils.is_user_authentic(request)
        if not is_user_authentic:
            return self.error_response(request,
                utils.get_error_response(
                'UnAuthorized',
                'User not allowed to access the resource'),
                HttpUnauthorized
            )
        request_parameters = request.GET
        project_id = request_parameters.get('project_id')
        employee_id = request.GET.get('employee_id')
        start_date = parser.parse(request_parameters.get('start_date'))
        end_date = parser.parse(request_parameters.get('end_date'))
        end_date = datetime.combine(
            end_date.date(),
            datetime.now().time().max
        )
        response_dict = analytics_operations.generate_task_table(
            project_id,
            employee_id,
            start_date,
            end_date
        )
        return self.create_response(request, response_dict)


    def set_slack_channel_id(self, request, **kwargs):
        is_user_authentic, user = utils.is_user_authentic(request)
        if not is_user_authentic or not utils.is_user_a_staff(user):
            return self.error_response(request,
                utils.get_error_response(
                'UnAuthorized',
                'User not allowed to access the resource'),
                HttpUnauthorized
            )
        request_parameters = utils.decode_json(request.body)
        employee_unique_id = request_parameters.get('user_id')
        channel_id = request_parameters.get('channel_id')
        try:
            employee = Employee.objects.get(unique_id=employee_unique_id)
            employee.slack_bot_channel_id = channel_id
            employee.save()
            return HttpResponse(status=200)
        except Employee.DoesNotExist:
            return HttpResponse(status=400)
