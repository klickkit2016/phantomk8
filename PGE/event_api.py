from datetime import datetime
from datetime import timedelta
from dateutil import parser

from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.http import HttpApplicationError
from tastypie.http import HttpBadRequest
from tastypie.http import HttpNotFound
from tastypie.http import HttpUnauthorized
from tastypie.resources import ModelResource
from tastypie.utils import trailing_slash

from django.conf.urls import include
from django.conf.urls import url
from django.http import HttpResponse

from PGE import analytics_operations
from PGE import constants
from PGE import operations
from PGE import utils
from PGE.models import Employee
from PGE.models import Project
from PGE.models import Team


class EventResource(ModelResource):
    class Meta:
        queryset = Team.objects.all()
        resource_name ='event'

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/slack%s$" %
            (self._meta.resource_name, trailing_slash()),
            self.wrap_view('handle_slack_event'),
            name="api_handle_slack_event"),
        ]

    def handle_slack_event(self, request, **kwargs):
        request_parameters = utils.decode_json(request.body)
        team_id = request_parameters.get("team_id")
        challenge_value = request_parameters.get("challenge")
        event_dict = request_parameters.get("event")
        if event_dict:
            event_type = event_dict["type"]
            if event_type == constants.APP_UNINSTALLED_EVENT:
                operations.delete_team(team_id)
            return HttpResponse(status=200)
        if challenge_value:
            response_dict = {"challenge": challenge_value}
            return self.create_response(request, response_dict)

