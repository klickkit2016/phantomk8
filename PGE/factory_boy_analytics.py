import factory

from PGE.models import Project
from PGE.models import Stage
from PGE.models import Task
from PGE.models import TaskLog


class ProjectFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Project

    project_name = "project_alpha"


class StageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Stage

    stage_name = "102"


class TaskFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Task

    task_name = "t1"
    stage = factory.SubFactory(StageFactory)
    project = factory.SubFactory(ProjectFactory)


class TaskLogFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = TaskLog

    task = factory.SubFactory(TaskFactory)

