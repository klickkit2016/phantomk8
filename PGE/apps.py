from __future__ import unicode_literals

from django.apps import AppConfig


class PGEConfig(AppConfig):
    name = 'PGE'
    verbose_name = ('phantom')

    def ready(self):
        from PGE import signals
