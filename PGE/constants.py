import pytz

# Host details
LOCAL_BOT_SERVER = "http://localhost:8000"
PRODUCTION_BOT_SERVER = "https://mighty-crag-57248.herokuapp.com"

ADMIN_GROUP_NAME = 'ADMIN_GROUP'
CLIENT_ID = "226243369125.230355708374"
CLIENT_SECRET = "cc68faaf9581f81003d31ef414050e85"
NUMBER_OF_DAYS_IN_WEEK = 7
DEVELOPER_ACCESS_TOKEN = "4b223639d2c742aaa9f4d7ab6eb31ec3"

HEADER_FOR_AGENT_MANIPULATION = {'Content-Type': 'application/json; charset=utf-8',
    'Authorization': 'Bearer {0}'.format(DEVELOPER_ACCESS_TOKEN)
}
headers = {'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer b55df5347afe4002a39e94cd61c121c9'}
SUCCESS_STATUS_CODE = 200
TASK_ENTITY_NAME = "Task"
EMPLOYEE_ENTITY_NAME = "employee"
PROJECT_ENTITY_NAME = "Project"
ENTITY_ADDITION_URL = "https://api.api.ai/v1/entities/{0}/entries?v=20150910"
TODO_STAGE_NAME = '101'
DOING_STAGE_NAME = '102'
DONE_STAGE_NAME = '103'
STRING_FOR_SPACES = '\s+'
REGULAR_NEW_SESSION = 'REG'
SWITCHED_NEW_SESSION = 'SWITCH'
STAGE_MAPPING = {TODO_STAGE_NAME: 'Not yet started',
    DOING_STAGE_NAME: 'In progress',
    DONE_STAGE_NAME: 'Completed'
}
TASK_SHIFTED_MESSAGE = 'Task(s) advanced sucessfully.'
tz = pytz.timezone('Asia/Kolkata')
ABSENCE_ACCEPTANCE_VALUE = "ABSENCE_YES"
ABSENCE_REJECTION_VALUE = "ABSENCE_NO"
QUERY_NO_MATCH_MESSAGE = 'QUERY_NO_MATCH'
DEFAULT_TIME_STRING = '10:00 AM'
DOES_NOT_EXIST_ERROR_CODE = 'DoesNotExist'
ADDITION_OPERATION = "ADD"
SUBTRACTION_OPERATION = "SUB"
ZERO_ETA_REMAINING = "ZETA"
NEGATIVE_ETA_REMAINING = "NGETA"
CONTEXTS_ENDPOINT = 'https://api.api.ai/v1/contexts?sessionId={0}'
SPECIFIC_CONTEXT_MODIFICATION_ENDPOINT = 'https://api.dialogflow.com/v1/contexts/{0}?sessionId={1}'
API_ERROR_CODE = 'errorCode'
API_ERROR_MESSAGE = 'errorMessage'
HOURS = "hr"
MINUTES = "min"
DAYS = "days"
STANDARD_COLOR_CODE = "#15328a"
TASK_TABLE_GET_PARAMETER_LIST = ["project_id", "employee_id", "start_date", "end_date"]
HUNDRED_PERCENT = 100
SESSIONS_BEFORE_NEXT_UPDATE = 3
DANGER_COLOR_CODE = "danger"
WARNING_COLOR_CODE = "warning"
GOOD_COLOR_CODE = "good"
APP_UNINSTALLED_EVENT = "app_uninstalled"
PROJECT_CREATION_NOTIFICATION_CALLBACK_ID = "PCN"
PATTERN_STRING_FOR_MATCHING_INTEGERS = "\d+"
CHEAT_SHEET_URL = "https://s3.ap-south-1.amazonaws.com/phantom-resources/cheat_sheet.png"
SESSION_START_CONTEXT = "session_start"


def get_meeting_creation_response(meet_date, meet_time, discussion_id):
    return "Discussion scheduled at {0} on {1} successfully. The discussion id is `{2}`."\
        .format(meet_time, meet_date, discussion_id)


def get_project_creation_notification_callback_id(employee_id, project_id):
    return "PCN-EMP{0}-PROJ{1}".format(employee_id, project_id)


def get_im_endpoint(token, user):
    return "https://slack.com/api/im.open?token={0}&user={1}&pretty=1".format(token, user)


def get_message_for_meeting(user_id_list):
    name_string = ""
    length_of_list = len(user_id_list)
    for index, user_id in enumerate(user_id_list):
        name_string = name_string + user_id
        if index != length_of_list - 1:
            name_string = name_string + ', '
        if index == length_of_list - 2:
            name_string = name_string + 'and '
    response = "You have a meeting scheduled to happen with {0}"\
        .format(name_string)
    return response


def get_meeting_id(project_name, discussion_id):
    return "{}MEET{}".format(project_name.upper(), discussion_id)


def message_for_task_not_in_doing_list(task_name):
    return "`{0}` doesn't exist in the list of tasks in progress under your working project."\
        .format(task_name)

def get_progress_prompt_message(task_name):
    return "Please tell me the progress in `{0}`".format(task_name)

def get_message_for_task_non_existence(task_name):
    return "`{0}` doesn't exist in the list of tasks under your working project."\
        .format(task_name)


def message_for_deadline_approach(task_name):
    return "Your deadline for `{0}` is approaching. You have one more hour. Speed Up!".format(task_name)


def title_for_interactive_message_for_absence_request():
    return "Are you okay with the absence?"


def work_time_display(task_name, work_time_string):
    sentence = "You have been working on `{}` for {} now.".format(task_name, work_time_string)
    return sentence


def get_response_message_for_addition_of_task_to_stage(next_stage_name, task_name):
    return "Task {0} is moved to the list of tasks that are {}.".\
        format(
            task_name,
            STAGE_MAPPING[next_stage_name]
        )


def response_for_task_already_completed_error(task_name):
    return 'Task `{0}` is already completed.'.format(task_name)


def response_for_currently_working_project(project_name):
    return "You are working on the project `{0}`.".format(project_name)


def get_absence_response_based_on_acceptance(did_accept=True):
    if not did_accept:
        return "Your manager is not okay with your absence."
    else:
         return "Your manager is okay with your absence."


def get_not_working_message():
    return "Sorry, you are not tracking work right now."


def get_eta_drain_warning_message(task):
    return "Hello! it looks like there's solid work done on `{0}`. Hey, I am keen to know how much time will it take to get this done!".format(task)


def get_deadline_skipped_message(task_name, employee):
    return "The deadline for {0} has been skipped by {1}".format(task_name, employee.user.first_name)


def get_task_description_header():
    return "Task Description"


def get_new_task_added_header():
    return "New task added!"


def get_next_task_suggestion(task_name, bound=None):
    base_response = "I think you should work on `{0}`".format(task_name)
    if bound:
        base_response = base_response + " for {0}.".format(bound)
    else:
        base_response = base_response + "."
    return base_response

def get_deadline_skipped_attachment(task_name, project_name, employee_name):
    return {
        "color": "danger",
        "title": "Deadline skipped",
        "fields": [
            {
                "title": "Task name",
                "value": task_name,
                "short": "false"
            },
            {
                "title": "Project Name",
                "value": project_name,
                "short": "false"
            },
            {
                "title": "Employee",
                "value": employee_name,
                "short": "false"
            }
        ],
        "footer": "Phantom Analytics"
    }


def get_links_detail_attachment(title, fields_list):
    return {
        "color": STANDARD_COLOR_CODE,
        "title": title,
        "fields": fields_list,
        "footer": "Phantom Links Archive"
    }


def get_cheetsheet_attachment():
    return  {
        "color": STANDARD_COLOR_CODE,
        "text": "Hey, you can do all these and much more!",
        "image_url": CHEAT_SHEET_URL
    }


def get_project_creation_payload(callback_id):
    return {
        "text": "Do you want to start working on it?",
        "callback_id": callback_id,
        "color": STANDARD_COLOR_CODE,
        "attachment_type": "default",
        "actions": [
            {
                "name": "switch_project_yes",
                "text": "Yes",
                "type": "button",
                "value": "switch_project_yes"
            },
            {
                "name": "switch_project_no",
                "text": "No",
                "type": "button",
                "value": "switch_project_no"
            }
        ]
    }


def get_task_detail_display_attachment(title, task_name, project_name, employee_name, deadline, eta, completion_value):
    payload =  {
        "color": STANDARD_COLOR_CODE,
        "title": title,
        "fields": [
            {
                "title": "Task name",
                "value": task_name,
                "short": "false"
            },
            {
                "title": "Project Name",
                "value": project_name,
                "short": "false"
            },
            {
                "title": "Employee",
                "value": employee_name,
                "short": "false"
            },
            {
                "title": "Deadline",
                "value": deadline,
                "short": "false"
            },
            {
                "title": "Required work time",
                "value": eta,
                "short": "false"
            },
            {
                "title": "Progress",
                "value": str(completion_value) + "%",
                "short": "false"
            }
        ],
        "footer": "Phantom Analytics"
    }
    return payload


def get_interactive_payload(text, attachment_list):
    payload_dict = {
        "text": text,
        "attachments": attachment_list
    }
    return payload_dict


def get_welcome_message():
    return "Hi, I am Phantom K8, I've been added to your team, lets play!"


def display_time_of_work(time):
    return "You've worked for {0}.".format(time)


def get_project_switch_message(project_name):
    return "You have started working on `{0}`.".format(project_name)


def get_task_creation_flow_message(task_name):
    return "Creating task `{0}`... Can you give me a deadline?".format(task_name)
