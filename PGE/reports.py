import csv

from datetime import date
from dateutil import parser

from django.contrib.auth.models import User
from django.db.models import Q

from PGE.models import Employee
from PGE.models import Session
from PGE.models import Task

import constants


def generate_report(employee, days=7):
    start_date_time = datetime.now() - timedelta(days=days)
    all_tasks = employee.task_set.filter(session__start_date_time__gte=start_date_time).prefetch_related('session_set')
    for task in all_tasks:
        task_name = task.task_name
        task_row_dict = {task_name: [task.stage.stage_name]}
        session_timedelta = timedelta(0, 0, 0)
        for session in task.session_set.filter(start_date_time__gte=start_date_time):
            if session.end_date_time:
                session_timedelta = session_timedelta + (timezone.localtime(session.end_date_time) - timezone.localtime(session.start_date_time))
            else:
                session_timedelta = session_timedelta + (timezone.now() - timezone.localtime(session.start_date_time))
        task


def compute_progress_score_for_week(employee):
    start_of_week_date_time = datetime.now() - timedelta(days=7)
    total_number_of_tasks_taken_up = Task.objects.filter(created_at__gte=start_of_week_date_time)
    total_number_of_tasks_done = Task.objects.filter(created_at__gte=start_of_week_date_time,
        stage=constants.DONE_STAGE_NAME
    )
    return (total_number_of_tasks_done / total_number_of_tasks_taken_up)
