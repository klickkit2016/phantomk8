from datetime import datetime
from django.utils import timezone

combinations = []
set_preference = False
intersection_tuple = (0, 0)


def return_min_and_max_date_tuples(date_tuple_one, date_tuple_two, index=0):
    if date_tuple_one[index].time() > date_tuple_two[index].time():
        return date_tuple_two, date_tuple_one
    elif date_tuple_two[index].time() > date_tuple_one[index].time():
        return date_tuple_one, date_tuple_two
    else:
        return date_tuple_one, date_tuple_two


def find_intersection(date_range_list):
    is_full_intersection = True

    intersection_tuple = date_range_list[0]
    for date_tuple in date_range_list[1:]:
        
        min_tuple, max_tuple = return_min_and_max_date_tuples(intersection_tuple, date_tuple)
        if max_tuple[0] < min_tuple[1]:
            start_date_time = max_tuple[0]
            min_end_date_tuple, max_end_date_tuple = return_min_and_max_date_tuples(min_tuple, max_tuple, index=1)
            min_date_time = min_end_date_tuple[1]
            intersection_tuple = (start_date_time, min_date_time)
        else:
            is_full_intersection = False
            break
    
    return (is_full_intersection, intersection_tuple)



# Basically we apply the nCr formula where we apply the r in reverse order
def generate_combinations(index_list,  n,  r, index, combination, i, date_range_list):
    global intersection_tuple
    global set_preference

    if index == r:
        date_tuple_list = []
        
        if not set_preference:
            for combination_index in combination:
                date_tuple_list.append(date_range_list[combination_index])
            
            is_intersecting, intersection_tuple = find_intersection(date_tuple_list)
            set_preference = is_intersecting
        return
        


    if i >= n:
        return 
    
    combination[index] = index_list[i]

    generate_combinations(index_list, n, r, index + 1, combination, i + 1, date_range_list)
    generate_combinations(index_list, n, r, index, combination, i + 1, date_range_list)

    

def intersection_helper(date_range_list):
    global combinations

    date_tuple_list = []
    size = len(date_range_list)
    index_list = [i for i in range(0, size)]
    r = size
    while not set_preference and r > 1:
        combination_list = [0 for i in range(0, r)]
        generate_combinations(index_list, size, r, 0, combination_list, 0, date_range_list)
        r = r - 1
    if set_preference:
        return intersection_tuple
    else:
        return None

    '''
    for combination in combinations:
        for i in combination:
            date_tuple_list.append(date_range_list[i])

    print(date_tuple_list)
    '''

start_time = datetime(2017, 2, 1, 10, 00, 17, 345, tzinfo=timezone.utc)

end_time = datetime(2017, 2, 1, 11, 00, 17, 345, tzinfo=timezone.utc)

a = (start_time, end_time)


start_time1 = datetime(2017, 2, 1, 10, 20, 17, 345, tzinfo=timezone.utc)

end_time1 = datetime(2017, 2, 1, 10, 40, 17, 345, tzinfo=timezone.utc)


b = (start_time1, end_time1)

start_time2 = datetime(2017, 2, 1, 10, 30, 17, 345, tzinfo=timezone.utc)

end_time2 = datetime(2017, 2, 1, 11, 00 , 17, 345, tzinfo=timezone.utc)

c = (start_time2, end_time2)






